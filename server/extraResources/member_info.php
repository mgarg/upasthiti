<?php
  
  session_start();
  include("../front_end/member.php");
  

  if(!empty($_SESSION['un'])) {
  require ("../includes/config.php");
  
  
    $email=$_SESSION['un'];
    $connect=mysqli_connect(HOST, USER, PASSWORD) or die("Couldn't connect!");  //Connect to server 
    mysqli_select_db($connect, "upasthiti") or die("Couldn't find db!");    //Select database

     // $email=$_POST['username'];
      $member=mysqli_query($connect,"SELECT * FROM registered_persons WHERE email = '{$email}'");
      $mem_info=mysqli_fetch_assoc($member);
      $name=$mem_info['name'];
      $roll_no=$mem_info['roll_no'];
      $inst_stud=$mem_info['inst_stud'];


      echo '<nav class="navbar navbar-inverse" id="mainNav">
      <div class="container-fluid">
        
          <div class="navbar-header">
              <a href="#" class="navbar-brand">UPASTHITI</a>
          </div>';


        echo '<div>
              <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">' . $name . '</a></li>
                    <li><a href="../includes/logout.php">Log Out</a></li>
                 
              </ul>
          </div>'; 

        $yr= date('Y');
        $mon=date('m');

  
        $day=date('d');
  

        if(intval(($mon)>=7 && intval($day)>20) && (intval($mon)<=11 && intval($day)<=30) ){        //Between July and Nov
          $semester=1;
          $yearfrom=$yr;
          $yearto=$yr+1;
    
        }
        elseif (intval($mon)==12 && intval($day)>=24) {               //Special case for December
          $semester=2;
          $yearfrom=$yr;
          $yearto=$yr+1;
    
    
        }
        elseif (intval($mon)>=1 && (intval($mon)<=4 && intval($day)<=30)) {      //Between January and April
          $semester=2;
          $yearfrom=$yr-1;
          $yearto=$yr;
        
        }
        else{                         
          $semester=3;
          $yearfrom=$yr-1;
          $yearto=$yr;
    
        }


        $year=$yearfrom . "-" . $yearto;  


       
        $courseSearch=mysqli_query($connect,"SELECT  courseno, course_id FROM registerStudentToCourse WHERE roll_no='{$roll_no}' AND (year='{$year}' AND semester='{$semester}') ");
        if(!$courseSearch){
          die("Error:= " . mysqli_error($connect));
        } 

        echo '<div>
          <ul class="nav navbar-nav">
              <!--<li><a href="#" onclick="clickDownload(this)">Download</a></li>-->
              <li><a href="record.csv" download >Download</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <span class="caret"></span></a>
              <ul class="dropdown-menu">';
        while($courseList = mysqli_fetch_assoc($courseSearch)){
          $registeredCourses[]=$courseList['course_id'];
          echo '<li> <a href="#" onclick="clickCourse(this)"  id= "' . $courseList['course_id'] .'">'. $courseList['courseno']  .'</a></li>';
          //echo '<li> <a href="#" addEventListener("click", "clickCourse")  id= "' . $courseList['course_id'] .'">'. $courseList['courseno']  .'</a></li>';
         

        }       

           
        echo    '</ul>
              </li>
            </ul>
        </div>';


          echo '</div>
                </nav>';



echo '  <div id ="chart"><!--<canvas id="myChart" width="600" height="400"></canvas>-->
        <center><div id="chartHeading" class="alert alert-info"><span id="CT"></span></div></center>
     <!--   <center><div id="stat3" class="alert"><h4><span id="statsValue3"></span></h4></div></center>
       <center><div id="stat1" class="alert alert-info"><h4>Present : <span id="statsValue1"></span></h4></div><div id="stat2" class="alert alert-info"><h4>Absent : <span id="statsValue2"></span></h4></div></center>-->
        <center><div id="canvas"><canvas id="myChart1" height="400"></canvas></div></center>
     <!--  <center><div id="chartTitle" class="alert"><a href="#" onclick="clickDownload(this)">Download</a> attendance statistics </div></center>-->
      </div> ';
/*echo '<div id="chart" style="width: 60px; height: 40px;"></div>';*/

//echo '<div id="downloadData" class="alert"><a href="#" onclick="clickDownload(this)">Download</a> attendance statistics </div>';
echo '<div id="calendar"></div>' ;
}
else{
  echo '
      <br/><br/><br/><center><div class="alert" id="logoutAlert" style="width:450px;">
        <strong>Error Loging in</strong> Click <a href="../front_end/login.php">here</a> to go to Log in page.
    </div></center>';
}

?>

<script>
var inst_stud=<?php echo json_encode($inst_stud); ?>;
var roll_no=<?php echo json_encode($roll_no); ?>;
var name=<?php echo json_encode($name); ?>;
var firstCourseId=<?php echo json_encode($registeredCourses[0]); ?>;
var requiredCourseId=parseInt(firstCourseId);
var myPichart;
var totClass=0;
var totPresent=0;
var myData;


$(document).ready(function() {

 
     $.ajax({
   url: 'extractStudentRecord.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + parseInt(roll_no),
   data: {
      format: 'json'
   },
   error: function() {
     
   },
   dataType: 'jsonp',
   success: function(data) {
    
   },
   type: 'GET'
});
 
    $('#calendar').fullCalendar({
     
      editable: true,
      eventLimit: true, 
      events: 'attendanceStudent.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + parseInt(roll_no),
      

      
      
    });

    json(requiredCourseId,roll_no);
  
 
               

   
  });

function clickCourse(el){
  
    requiredCourseId=parseInt(el.id);
    $.ajax({
   url: 'extractStudentRecord.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + parseInt(roll_no),
   data: {
      format: 'json'
   },
   error: function() {
     
   },
   dataType: 'jsonp',
   success: function(data) {
    
   },
   type: 'GET'
});
    setCalender(requiredCourseId,roll_no);
}

function json(requiredCourseId,roll_no){  

    //alert(requiredCourseId);
    $.getJSON('attendanceStudentTotal.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + roll_no, function(jd) {

               Cno = jd[0]['courseNo'];
               Cname = jd[0]['courseName'];
               totPresent = jd[0]['totPresent']; 
               totClass = jd[0]['totClass'];
               totAbsent = totClass - totPresent;

               
              google.setOnLoadCallback(drawPieChart(totPresent,totAbsent, totClass, Cno, Cname));
               
               });

}


function drawPieChart(tp,ta,tc,cno,cname){

  
  //alert(tp);

  var data = google.visualization.arrayToDataTable([
                    ['Status', 'No of Days'],['Present',tp*1],['Absent',ta] ]);

  document.getElementById('CT').innerHTML = "<h4>YOUR ATTENDANCE STATISTICS FOR </h4><br><h4>" + cname + " (" + cno + ")</h4>"; 
  


  new google.visualization.PieChart(document.getElementById('canvas')).
                draw(data, {title:""});

   
}

function setCalender(requiredCourseId,roll_no){


  $('#calendar').fullCalendar('removeEvents');
  $('#calendar').fullCalendar( 'addEventSource', 'attendanceStudent.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + parseInt(roll_no));
  json(requiredCourseId,roll_no);
  
}



</script>


<?php
  echo '</body></html>'

?>