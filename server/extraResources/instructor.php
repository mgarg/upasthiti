<?php
  
  session_start();
  include("../front_end/member.php");
  

  if(!empty($_SESSION['un'])) {
  require ("../includes/config.php");
  
  
    $email=$_SESSION['un'];
    $connect=mysqli_connect(HOST, USER, PASSWORD) or die("Couldn't connect!");  //Connect to server 
    mysqli_select_db($connect, "upasthiti") or die("Couldn't find db!");    //Select database

     // $email=$_POST['username'];
      $member=mysqli_query($connect,"SELECT * FROM registered_persons WHERE email = '{$email}'");
      $mem_info=mysqli_fetch_assoc($member);
      $name=$mem_info['name'];
      $roll_no=$mem_info['roll_no'];
      $inst_stud=$mem_info['inst_stud'];


      echo '<nav class="navbar navbar-inverse" id="mainNav">
      <div class="container-fluid">
        
          <div class="navbar-header">
              <a href="#" class="navbar-brand">UPASTHITI</a>
          </div>';


        echo '<div>
              <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">' . $name . '</a></li>
                    <li><a href="../includes/logout.php">Log Out</a></li>
                 
              </ul>
          </div>'; 

        $yr= date('Y');
        $mon=date('m');

  
        $day=date('d');
  

        if(intval(($mon)>=7 && intval($day)>20) && (intval($mon)<=11 && intval($day)<=30) ){        //Between July and Nov
          $semester=1;
          $yearfrom=$yr;
          $yearto=$yr+1;
    
        }
        elseif (intval($mon)==12 && intval($day)>=24) {               //Special case for December
          $semester=2;
          $yearfrom=$yr;
          $yearto=$yr+1;
    
    
        }
        elseif (intval($mon)>=1 && (intval($mon)<=4 && intval($day)<=30)) {      //Between January and April
          $semester=2;
          $yearfrom=$yr-1;
          $yearto=$yr;
        
        }
        else{                         
          $semester=3;
          $yearfrom=$yr-1;
          $yearto=$yr;
    
        }


        $year=$yearfrom . "-" . $yearto;  


       
        $courseSearch=mysqli_query($connect,"SELECT  courseno, course_id, coursename FROM course_register WHERE email = '{$email}' AND (year='{$year}' AND semester='{$semester}' )");
        if(!$courseSearch){
          die("Error:= " . mysqli_error($connect));
        } 

        echo '<div>
          <ul class="nav navbar-nav">
              <!--<li><a href="#" onclick="clickDownload(this)">Download</a></li>-->
              <li><a href="record.csv" download >Download</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <span class="caret"></span></a>
              <ul class="dropdown-menu">';
        $totalCourses=0;
        while($courseList = mysqli_fetch_assoc($courseSearch)){
          $registeredCourses[]=$courseList['course_id'];
          $courseNameList[]=$courseList['coursename'];
          $courseNoList[]=$courseList['courseno'];
          $totalCourses=$totalCourses+1;
          echo '<li> <a href="#" onclick="clickCourse(this)"  id= "' . $courseList['course_id'] .'">'. $courseList['courseno']  .'</a></li>';

         

        }       

           
        echo    '</ul>
              </li>
            </ul>
        </div>';


          echo '</div>
                </nav>';


echo '<div class="container-fluid">
       <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <div class="panel panel-default">
          
            <div class="page-header"  style="margin-top:2px; padding:2px;">
              <span id="courseDet"></span>
            </div>
              
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
    </div>';   



echo '
  <div id="left">
  <center><div id="listHeading" class="alert alert-info"><h4><span id="studentField1">List of registered students</span></h4><h5>(<span id="studentField2">reds show students with <75% attendance</span>)</h5><br/><input id="searchStudents" placeholder="Search by name"></div></center>
  <center></center>
  <div id="tabs-left">
  <div class="tabbable tabs-left" id="courseListContainer"> <!-- Only required for left/right tabs -->
    
    <div id="studentListDiv" style="width:100%"><ul class="nav nav-pills nav-stacked" id="studentList">';




 echo '</ul></div></div></div></div>';   



echo ' <div id ="chart2"><!--<canvas id="myChart" width="600" height="400"></canvas>-->
        <center><div id="chartHeading" class="alert alert-info"><span id="CT"></span></div></center>
     <!--   <center><div id="stat3" class="alert"><h4><span id="statsValue3"></span></h4></div></center>
       <center><div id="stat1" class="alert alert-info"><h4>Present : <span id="statsValue1"></span></h4></div><div id="stat2" class="alert alert-info"><h4>Absent : <span id="statsValue2"></span></h4></div></center>-->
        <center><div id="canvas"><canvas id="myChart1" height="400"></canvas></div></center>
     <!--  <center><div id="chartTitle" class="alert"><a href="#" onclick="clickDownload(this)">Download</a> attendance statistics </div></center>-->
      </div> ';
/*echo '<div id="chart" style="width: 60px; height: 40px;"></div>';*/

//echo '<div id="downloadData" class="alert"><a href="#" onclick="clickDownload(this)">Download</a> attendance statistics </div>';
echo '<div id="calendar"></div>' ;




}
else{
  echo '
      <br/><br/><br/><center><div class="alert" id="logoutAlert" style="width:450px;">
        <strong>Error Loging in</strong> Click <a href="../front_end/login.php">here</a> to go to Log in page.
    </div></center>';
}

?>

<script>
var inst_stud=<?php echo json_encode($inst_stud); ?>;
//var roll_no=<?php echo json_encode($roll_no); ?>;
var name=<?php echo json_encode($name); ?>;
var firstCourseId=<?php echo json_encode($registeredCourses[0]); ?>;
var firstCourseName=<?php echo json_encode($courseNameList[0]); ?>;
var firstCourseNo=<?php echo json_encode($courseNoList[0]); ?>;
var requiredCourseId=parseInt(firstCourseId);
var courseName;
var courseNo;
var myPichart;
var totClass=0;
var totPresent=0;
var myData;


$(document).ready(function() {


   

     document.getElementById('courseDet').innerHTML="<h4>" + firstCourseName + " (" + firstCourseNo + ")</h4>";

     jsonStudentList(requiredCourseId);

     jsonLineChart(requiredCourseId);


 
    $('#calendar').fullCalendar({
      editable: false,
      eventLimit: true, 
      events: 'attendanceOfStudents.php?course_id='+ requiredCourseId,
      eventClick: function(calEvent, jsEvent, view){
        var values=calEvent.title.split(" out of ");
        var numberOfStudentsPresent=parseInt(values[0]);
        var numberOfStudentsAbsent=parseInt(values[1])-numberOfStudentsPresent;
        var date=calEvent.start;

        
       
       
       document.getElementById('CT').innerHTML = "<h5>ATTENDANCE STATISTICS FOR </h5><br><h5>" + moment(date).format('YYYY-MM-DD') +"</h5>";

       var data = google.visualization.arrayToDataTable([
                    ['Status', 'No of Students'],['Present',numberOfStudentsPresent],['Absent',numberOfStudentsAbsent] ]);

        new google.visualization.PieChart(document.getElementById('canvas')).
                draw(data, {title:""});

      },
          error: function () {
                alert('there was an error while fetching events!');
            }

      
      
    });

    
  /*  $.getJSON('attendanceOfStudents.php?course_id='requiredCourseId, function(data){

    });*/


   

    
   

   
  });


function jsonLineChart(requiredCourseId){
 $.getJSON('attendanceOfStudents.php?course_id=' + requiredCourseId, function(data){

      var values=[['Date', 'Number of Students Present']];
      var temp;
      var check=0;

      for (var i = 0; i < Object.keys(data).length; i++) {
        temp=[];
        var numberOfStudentsPresent=parseInt(data[i]['title'].split(" out of "));
        var date=moment(data[i]['start']).format('DD/MM');

        temp.push(date);
        temp.push(numberOfStudentsPresent);
        
        values.push(temp);
        check=1;
      }

      if(check==1){
      var data2=google.visualization.arrayToDataTable(values);

       var options = {
          legend: { position: 'bottom' },
          colors:['green']
        };

      new google.visualization.LineChart(document.getElementById('canvas')).draw(data2,options);

      document.getElementById('CT').innerHTML = "<h5>ATTENDANCE TREND </h5>";//<br><h5>" + requiredCourseId + "</h5>";

    }
    else{
     
      document.getElementById('CT').innerHTML = "<h5>Attendance not taken </h5>";
    }


    });

}

function clickCourse(el){
  
    requiredCourseId=parseInt(el.id);

    jsonStudentList(requiredCourseId);

    setCalender(requiredCourseId);

    //jsonCourseDet(requiredCourseId); 
    $.getJSON('extractCourseDet.php?course_id=' + requiredCourseId, function(data){

    courseNAME=data[0]['courseName'];
    courseNO=data[0]['courseNo'];
    alert(courseName);

    document.getElementById('courseDet').innerHTML="<h4>" + courseNAME + " (" + courseNO + ")</h4>";

  });

    jsonLineChart(requiredCourseId);

}

function clickStudent(el){

  var roll_no=parseInt(el.id);

  
 setCalenderStud(requiredCourseId,roll_no);
  $.getJSON('attendanceStudentTotal.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + roll_no, function(jd) {

              // Cno = jd[0]['courseNo'];
               //Cname = jd[0]['courseName'];
               totPresent = jd[0]['totPresent']; 
               totClass = jd[0]['totClass'];
               totAbsent = totClass - totPresent;

              // alert(totPresent);
             // document.getElementById('CT').innerHTML = "<h4>ATTENDANCE STATISTICS FOR </h4><br><h4>" + rno + "</h4>"; 
              var data = google.visualization.arrayToDataTable([
                    ['Status', 'No of Students'],['Present',totPresent*1],['Absent',totAbsent] ]);
              new google.visualization.PieChart(document.getElementById('canvas')).
                draw(data, {title:""});

              document.getElementById('CT').innerHTML = "<h5>ATTENDANCE STATISTICS FOR </h5><br><h5>" + roll_no + "</h5>"; 

              //google.setOnLoadCallback(drawPieChart(totPresent,totAbsent));
               
               });


}

function jsonCourseDet(requiredCourseId){

  //alert(requiredCourseId);

  $.getJSON('extractCourseDet.php?course_id=' + requiredCourseId, function(data){
    var courseName=data[0]['courseName'];
    var courseNo=data[0]['courseNo'];
    

    document.getElementById('courseDet').innerHTML="<h4>" + courseName + " (" + courseNo + ")</h4>";

  });
}

function jsonStudentList(requiredCourseId){  

  
  $.getJSON('extractStudentList.php?course_id='+ requiredCourseId, function(data) {

  var ListOfStud = new String(); 
 // alert(Object.keys(data).length);
  
  for (var i = 0; i < Object.keys(data).length; i++) {
      ListOfStud = ListOfStud + '<li><a href="#" onclick="clickStudent(this)" id="'+ data[i]['roll_no'] +'" data-toggle="tab">'+data[i]['name']+' ('+data[i]['roll_no']+')</a></li>';
        
  }

  document.getElementById('studentList').innerHTML=ListOfStud;
              
               
               });

}


function drawPieChart(tp,ta){

  
  

  var data = google.visualization.arrayToDataTable([
                    ['Status', 'No of Students'],['Present',tp*1],['Absent',ta] ]);

//document.getElementById('CT').innerHTML = "<h5>ATTENDANCE STATISTICS FOR </h5><br><h5>" + moment(date).format('YYYY-MM-DD') +"</h5>";
  


  new google.visualization.PieChart(document.getElementById('canvas')).
                draw(data, {title:""});

   
}




function setCalender(requiredCourseId){

  $('#calendar').fullCalendar('removeEvents');
  $('#calendar').fullCalendar( 'addEventSource', 'attendanceOfStudents.php?course_id='+ requiredCourseId);
 // $('#calendar').fullCalendar( 'refetchEvents' );
 }

function setCalenderStud(requiredCourseId,roll_no){


  $('#calendar').fullCalendar('removeEvents');
  $('#calendar').fullCalendar( 'addEventSource', 'attendanceStudent.php?course_id='+ requiredCourseId + "&" + 'roll_no=' + parseInt(roll_no));
 


  
}



</script>


<?php
  echo '</body></html>'

?>