<?php
	
	$myfile = fopen("/home/sandipan/Documents/request.txt", "a") or die("Unable to open file!");
	$time=date('Y-m-d H:i:s');
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		fwrite($myfile, "Received post request for registerCourse at " . $time . "\n");
    
	}	

	require ("../includes/config.php");  //Including config file to get username and password

	$connect=mysqli_connect(HOST,USER,PASSWORD) or die("Couldn't connect!");	//Connect to server	
	mysqli_select_db($connect, "upasthiti") or die("Couldn't find db!");		//Select database


	//Checking if the email id exists or not

	$email=$_POST['email'];
	$email_validity=mysqli_query($connect,"SELECT count(email) as total FROM registered_persons WHERE `email`='{$email}'");
	$row = mysqli_fetch_assoc($email_validity);


	//If so then only insert fingerprint
	if($row['total']>0)
	{
		echo "email id is valid\n";

		//Checking if it belongs to an instructor
		$inst=mysqli_query($connect,"SELECT * FROM registered_persons WHERE email='{$email}'");

		$check=mysqli_fetch_assoc($inst);

		
		
		if(strcasecmp($check['inst_stud'],'Instructor')==0)
		{

			echo "email id is of an instructor\n";

			if($check['fingerprint']!=''){
				echo "fingerprint registered\n";

				//Autofilling year and semester
				$yr= date('Y');
				$mon=date('m');
				$day=date('d');
	

				if(intval(($mon)>=7 && intval($day)>20) && (intval($mon)<=11 && intval($day)<=30) ){				//Between July and Nov
					$semester=1;
					$yearfrom=$yr;
					$yearto=$yr+1;
		
				}
				elseif (intval($mon)==12 && intval($day)>=24) {								//Special case for December
					$semester=2;
					$yearfrom=$yr;
					$yearto=$yr+1;
		
		
				}
				elseif (intval($mon)>=1 && (intval($mon)<=4 && intval($day)<=30) {			//Between January and April
					$semester=2;
					$yearfrom=$yr-1;
					$yearto=$yr;
				
				}
				else{													
					$semester=3;
					$yearfrom=$yr-1;
					$yearto=$yr;
		
				}


				$year=$yearfrom . "-" . $yearto;
				$courseno=$_POST['courseno'];
				$coursename=$_POST['coursename'];

	

				//Inserting into table
				$result=mysqli_query($connect,"INSERT INTO course_register (email,courseno, coursename, year, semester) VALUES ('{$email}', '{$courseno}','{$coursename}','{$year}','{$semester}')");
	
	


				//Checking if sql query was a success or failure
				if($result){
					echo "Success";
				}

				else{
					echo "Failure";
				}

			}

			else{
				echo "fingerprint not registered\n";
			}

		
	}

	else{
		echo "email id not of an instructor\n";
	}

}

else{
	echo "email id is invalid\n";
}

	
?>