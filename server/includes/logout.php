
<!--<html>
<head>
<link href="bootstrapAssets/css/bootstrap.css" rel="stylesheet">
<link rel='stylesheet' href='includes/fullcalendar.min.css' />
<link rel="stylesheet" href="main.css">
<link rel="stylesheet" href="includes/jquery-ui.min.css">

<script type="text/javascript" src="includes/jquery.min.js"></script>
<script src="includes/jquery-ui.min.js"></script>
<script type="text/javascript" src="bootstrapAssets/js/bootstrap.min.js"></script>
<script src='includes/moment.min.js'></script>
<script src='includes/fullcalendar.min.js'></script>
<script src="includes/Chart.min.js"></script>
</head>
</body>-->
<?php
	session_start();
	include("../front_end/member.php");
	require("config.php");
	if(!empty($_SESSION['un']))
	{
		$email=$_SESSION['un'];
		$loginDate=$_SESSION['loginDate'];
		$loginTime=$_SESSION['loginTime'];

		session_unset();
		session_destroy();
		//setcookie("username","", time()-3600);
		$con=mysqli_connect(HOST,USER,PASSWORD) or die("Couldn't connect to server");
		mysqli_select_db($con,"upasthiti");


		$logoutDate=date('Y-m-d');
		$logoutTime=date('H:i:s');
		mysqli_query($con,"UPDATE loginLog SET logoutDate='{$logoutDate}', logoutTime='{$logoutTime}' WHERE loginDate='{$loginDate}' AND loginTime='{$loginTime}' ");

		
		echo '
			<br/><br/><br/><center><div class="alert" id="logoutAlert" style="width:450px;">
	  		<strong>You have been successfully logged out.</strong> Click <a href="../front_end/login.php">here</a> to go to Log in page.
		</div></center>';
	}
	else
		echo '
			<br/><br/><br/><center><div class="alert alert-danger">Error!</div></center>
		';
?>
