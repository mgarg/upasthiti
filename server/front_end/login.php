<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Log In</title>

    <!-- Bootstrap -->
    <link href="../bootstrap_resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap_resources/css/bootstrap.css" rel="stylesheet">
    <script src = "./bootstrap_resources/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>

  <nav class="navbar navbar-inverse" id="mainNav">
    <div class="container-fluid">
        
        <div class="navbar-header">
          <a href="http://localhost/front_end/head.php" class="navbar-brand">UPASTHITI</a>
        </div>

        <div>
          <ul class="nav navbar-nav" id="miscLinks">
            <li class="active"><a href="http://localhost/front_end/login.php">LOG IN</a></li>
            <li><a href="http://localhost/front_end/register.php">REGISTER</a></li>
        </ul>
        </div>

        <div>
            <ul class="nav navbar-nav navbar-right">
                  <li><a href="http://www.iitk.ac.in">IITK</a></li>
                  <li><a href="http://www.cse.iitk.ac.in">IITK CSE</a></li>
            </ul>
        </div>
       
        
    </div>
    </nav>

  <p><br/></p>
  <p><br/></p>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header" style="margin-top:15px;">
              <h2>Log in to your Account</h2>
            </div>
            <form class="form-horizontal" method="post">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                      <input type="text" class="form-control" id="inputEmail3" placeholder="Username" name="username">
                        <span class="input-group-addon">@iitk.ac.in</span>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span> 
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="pswd">
                    </div>
                </div>
              </div>

              
  
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary" name="login">Log In</button>
                 <!-- <button type="submit" class="btn btn-success">Register Yourself</button>-->
                </div>
              </div>
          </form>


          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
  </div>

  <?php
    require ("../includes/loginCheck.php");
  ?>
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed >-->
    <script src="../bootstrap_resources/js/bootstrap.min.js"></script>
  </body>
</html>

