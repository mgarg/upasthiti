<!DOCTYPE html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Register Yourself</title>

    <!-- Bootstrap -->
    <link href="../bootstrap_resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap_resources/css/bootstrap.css" rel="stylesheet">
    <script src = "./bootstrap_resources/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>

  <nav class="navbar navbar-inverse" id="mainNav">
    <div class="container-fluid">
        
        <div class="navbar-header">
          <a href="http://localhost/front_end/head.php" class="navbar-brand">UPASTHITI</a>
        </div>

        <div>
          <ul class="nav navbar-nav" id="miscLinks">
            <li><a href="http://localhost/front_end/login.php">LOG IN</a></li>
            <li class="active"><a href="http://localhost/front_end/register.php">REGISTER</a></li>
          </ul>
        </div>


        <div>
            <ul class="nav navbar-nav navbar-right">
                 <li><a href="http://www.iitk.ac.in">IITK</a></li>
                 <li><a href="http://www.cse.iitk.ac.in">IITK CSE</a></li>
            </ul>
        </div>
       
        
    </div>

        
  </nav>


  <p><br/></p>
  <p><br/></p>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header" style="margin-top:15px;">
              <h2>Please Fill in the Details</h2>
            </div>
            <form name="register" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                      <input type="text" class="form-control" id="inputEmail3" placeholder="Username" name="username" >
                        <span class="input-group-addon">@iitk.ac.in</span>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span> 
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="pswd" >
                    </div>
                </div>
              </div>

              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span> 
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="cnfpswd" >
                    </div>
                </div>
              </div>
  
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="rg" class="btn btn-primary">Register</button>
                 <!-- <button type="submit" class="btn btn-success">Log In Page</button>-->
                </div>
              </div>
          </form>
           </div>
        </div>
      </div>
      <div class="col-md-2"></div>
  </div>

   

<?php 
//session_start();


if(isset($_POST["rg"]))
{
  require("../includes/config.php");
  if($_POST['pswd']== ''){
   // $_SESSION["stat"]=1;
    $msg=1;
    //die();
  }

  elseif(!strcmp($_POST["pswd"],$_POST["cnfpswd"])) {
  //  $_SESSION["stat"]=2;
    $connect=mysqli_connect(HOST, USER, PASSWORD) or die("Couldn't connect!");  //Connect to server 
   
   

    mysqli_select_db($connect, "upasthiti") or die("Couldn't find db!");    //Select database

    $pwd=md5($_POST["pswd"]);
    $usr=$_POST["username"];

    $check=mysqli_query($connect,"SELECT name,fingerprint FROM registered_persons WHERE email = '{$usr}' ");
    $num=mysqli_num_rows($check);
    $values=mysqli_fetch_assoc($check);

    if(($num>0 && $values['fingerprint']=="")|| $num==0 )
    {
      $msg=2;
      //die();
    }



    else{
      $name=$values['name'];
      $checkReg=mysqli_query($connect,"SELECT * FROM onlineRegistration WHERE email='{$usr}' ");
      if(mysqli_num_rows($checkReg)==0){
      $result=mysqli_query($connect,"INSERT INTO onlineRegistration(name,email,password) VALUES ('{$name}','{$usr}','{$pwd}')");
      $msg=3;
      //echo "Success";
    }
      else{
        $msg=4;
       // die("Already Registered");
      }
    }
  }
  else{
   // $_SESSION["stat"]=3;
    $msg=5;
  }
    

}

?>

<script type="text/javascript">

  var msg=<?php echo $msg; ?>;
  if (msg==1){
    alert("Blank Password not allowed");
  }
  else if(msg==2){
    alert("Register with your fingerprint first");
  }
  else if(msg==3){
    alert("You are successfully registered");
    
  }
  else if(msg==4){
    alert("You are already registered");
  }
  else{
    alert("Passwords don't match")
  }
  
</script>

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed >-->
    <script src="../bootstrap_resources/js/bootstrap.min.js"></script>

  </body>
</html>