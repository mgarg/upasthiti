# README FOR UPASTHITI REPOSITORY #

## Upasthiti Attendance System ##

* Quick summary
This project enables attendance by biometrics. A mobile app is used to take attendance, server-side handles all requests as well as implements a web interface for viewing attendance results.

## How do I get set up? ##

* ### Summary of set up ###
The repo has a <mobile> directory and a <server> directory. Use contents of <mobile> directory to build the app.
* ### Configuration ###
Use android studio to build the app.
* ### Dependencies ##
Requires FDXSDKProAndroid.jar library. 

## Who do I talk to? ##

* Repo owner or admin