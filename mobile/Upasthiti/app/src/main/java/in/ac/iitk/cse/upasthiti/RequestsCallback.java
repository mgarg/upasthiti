package in.ac.iitk.cse.upasthiti;

/**
 * Created by soumya on 21/6/15.
 */
public interface RequestsCallback {
    public abstract void done(boolean b);
    public abstract void done(User e);
    public abstract void done(String s);

}
