package in.ac.iitk.cse.upasthiti;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by soumya on 21/6/15.
 */
public class AsyncRequests {
    ProgressDialog progressDialog;
    public static final int REQUEST_TIMEOUT=15*1000;
    public static final String SERVER_ADDRESS="http://172.27.5.71/api/";
    public AsyncRequests(Context context, String title, String text){
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage(text);
        progressDialog.setTitle(title);
        progressDialog.setCancelable(true);
    }
//    public void getCourseList(LocalDBHelper db,RequestsCallback callback){
//        progressDialog.show();
//        new handleGetCourseList(db,callback).execute();
//    }
    public void handleFileFetch(LocalDBHelper db,BufferedReader bufferedReader,String course,Context context,RequestsCallback callback){//inputStream should have getResources().openRawResource(R.raw.student_data)
        progressDialog.show();
        new handleFileFetch(db,bufferedReader,course,callback,context).execute();
    }
    public void viewCourseStudents(String course, LocalDBHelper db, RequestsCallback callback){
        progressDialog.show();
        new handleViewStudents(course,db,callback).execute();
    }
    public void addStudentToCourse(int rollNumber,String course,LocalDBHelper db,Context context,RequestsCallback callback){
        progressDialog.show();
        new handleAddStudent(rollNumber,course,db,callback,context).execute();
    }
    public void fetchEmailDetails(String email, RequestsCallback callback){
        progressDialog.show();
        new handleEmailFetch(email,callback).execute();
    }
    public void sendUserData(User user,LocalDBHelper db,Context context,RequestsCallback callback){
        progressDialog.show();
        new handleSendUserData(user,callback,db,context).execute();
    }
    public void sendCourseData(Course course,LocalDBHelper db,Context context,RequestsCallback callback){
        progressDialog.show();
        new handleSendCourseData(course,callback,db,context).execute();
    }

    public class handleViewStudents extends AsyncTask<Void,Void,String>{

        LocalDBHelper dbHelper;
        String course;
        RequestsCallback callback;
        public handleViewStudents(String c,LocalDBHelper db, RequestsCallback callback) {
            this.course=c;
            this.dbHelper=db;
            this.callback=callback;
        }

        @Override
        protected String doInBackground(Void... params) {
            Cursor cursor=dbHelper.getStudentList(course);
            if(cursor.getCount()==0)
                return "No students here";
            String buffer="";
            buffer+=cursor.getCount()+",";
            while (cursor.moveToNext()){
                buffer+=cursor.getInt(cursor.getColumnIndex("roll_no"))+",";
                buffer+=dbHelper.nameOfRoll(cursor.getInt(cursor.getColumnIndex("roll_no")))+",";
//                dbHelper.addWord(cursor.getInt(cursor.getColumnIndex("roll_no")),dbHelper.nameOfRoll(cursor.getInt(cursor.getColumnIndex("roll_no"))));
            }

            return buffer;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            callback.done(s);
            super.onPostExecute(s);
        }
    }
    public class handleSendCourseData extends AsyncTask<Void,Void,Boolean>{

        Course course;
        RequestsCallback callback;
        SharedPreferences pref;
        LocalDBHelper db;
        public handleSendCourseData(Course course,RequestsCallback callback,LocalDBHelper db,Context context){
            this.course=course;
            this.callback=callback;
            this.db=db;
            this.pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            HttpURLConnection con = null;
            try {
//                if(db.insertDataCourse(course.courseId, course.courseName, course.instructor))
//                    Log.d("tag","insert in local db ok");
//                else
//                    return false;
//                con = (HttpURLConnection) ( new URL("registerCourse.php")).openConnection();
//                con.setRequestMethod("POST");
//                con.setDoInput(true);
//                con.setDoOutput(true);
//                con.connect();
                con = (HttpURLConnection) ( new URL("http://172.27.26.110/upasthiti/api/node")).openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
//                Log.d("TAGn", pref.getString("session_name", null) + ":" + pref.getString("session_id", null) + "&&" + pref.getString("token", null));
                con.setRequestProperty("Cookie",pref.getString("session_name", null)+"="+pref.getString("session_id", null));
                con.setRequestProperty("X-CSRF-Token", pref.getString("token", null));
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();
//                String fingerprint;roll_no,department,fingerprint
//                Toast.makeText(RegisterPerson.this, "Fetch from CSV File successful", Toast.LENGTH_SHORT).show();
//                Log.d("tag", "stage 1");
//                String data = URLEncoder.encode("courseno", "UTF-8") + "=" + URLEncoder.encode(course.courseId, "UTF-8");
//                data += "&" + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(course.instructor, "UTF-8");
//                data += "&" + URLEncoder.encode("coursename", "UTF-8") + "=" + URLEncoder.encode(course.courseName, "UTF-8");
//                Log.d("tag", "stage 2");
//                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//                wr.write(data);
//                wr.flush();
//                wr.close();
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("type", "course");
                jsonObject.put("title", course.courseName);
//                jsonObject.put("mail", user.email+"@iitk.ac.in");
//                    JSONObject temp1=new JSONObject();
//                        JSONArray tempArray1=new JSONArray();
//                            JSONObject tempArray1Obj=new JSONObject();
//                            tempArray1Obj.put("target_id","CS345");//dummy value
//                        tempArray1.put(tempArray1Obj);
//                    temp1.put("und",tempArray1);
//                    jsonObject.put("og_user_node",temp1);
//                JSONObject temp2=new JSONObject();
//                JSONArray tempArray2=new JSONArray();
//                JSONObject tempArray2Obj=new JSONObject();
//                tempArray2Obj.put("value", user.roll+"");
//                tempArray2.put(tempArray2Obj);
//                temp2.put("und",tempArray2);
//                jsonObject.put("field_rollno", temp2);
//                jsonObject.put("fingerprint", user.fingerprint);
                OutputStreamWriter printout=new OutputStreamWriter(con.getOutputStream());
                printout.write(jsonObject.toString());
                printout.flush();
                printout.close();

                InputStream inputStream = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream.close();
                String result = stringBuilder.toString();

                Log.d("TAG", con.getResponseCode() + "");
                JSONObject jsonObject1=new JSONObject(result);
                int nid=jsonObject1.getInt("nid");
                if(db.insertDataCourse(nid,course.courseId, course.courseName, course.instructor))
                    Log.d("tag", "insert data in localdb okay");
                else
                    return false;

                return true;

//                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
//                outputStreamWriter.write(data);
//                Log.d("tag", "stage 3");
//                outputStreamWriter.flush();
//                Log.d("tag", "stage 4");
//                outputStreamWriter.close();
            } /*catch (IOException e) {

            }*/
            catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean b) {
            progressDialog.dismiss();
            callback.done(b);
            super.onPostExecute(b);
        }
    }
    public class handleSendUserData extends AsyncTask<Void,Void,Boolean>{

        LocalDBHelper db;
        User user;
        SharedPreferences pref;
        RequestsCallback callback;
        public handleSendUserData(User user,RequestsCallback callback,LocalDBHelper db,Context context){
            this.user=user;
            this.callback=callback;
            this.db=db;
            this.pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        }

        @Override
        public Boolean doInBackground(Void... params) {
            HttpURLConnection con;
            try {

                con = (HttpURLConnection) ( new URL("http://172.27.26.110/upasthiti/api/user/register")).openConnection();
                con.setRequestMethod("POST");
//                con.setRequestProperty("Content-Type", "application/json");
                Log.d("TAGn", pref.getString("session_name", null) + ":" + pref.getString("session_id", null) + "&&" + pref.getString("token", null));
                con.setRequestProperty("Cookie",pref.getString("session_name", null)+"="+pref.getString("session_id", null));
                con.setRequestProperty("X-CSRF-Token", pref.getString("token", null));
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();

                JSONObject jsonObject=new JSONObject();
                    jsonObject.put("name", user.name);
                    jsonObject.put("pass", user.name);
                    jsonObject.put("mail", user.email+"@iitk.ac.in");
//                    JSONObject temp1=new JSONObject();
//                        JSONArray tempArray1=new JSONArray();
//                            JSONObject tempArray1Obj=new JSONObject();
//                            tempArray1Obj.put("target_id","CS345");//dummy value
//                        tempArray1.put(tempArray1Obj);
//                    temp1.put("und",tempArray1);
//                    jsonObject.put("og_user_node",temp1);
                    JSONObject temp2=new JSONObject();
                        JSONArray tempArray2=new JSONArray();
                            JSONObject tempArray2Obj=new JSONObject();
                            tempArray2Obj.put("value", user.roll+"");
                        tempArray2.put(tempArray2Obj);
                    temp2.put("und",tempArray2);
                    jsonObject.put("field_rollno", temp2);
                    jsonObject.put("fingerprint", user.fingerprint);
                OutputStreamWriter printout=new OutputStreamWriter(con.getOutputStream());
                printout.write(jsonObject.toString());
                printout.flush();
                printout.close();

                InputStream inputStream = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream.close();
                String result = stringBuilder.toString();

//                String data = URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(user.name, "UTF-8");
//                data += "&" + URLEncoder.encode("inst_stud", "UTF-8") + "=" + URLEncoder.encode((user.isInstructor)?"Instructor":"Student", "UTF-8");
//                data += "&" + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(user.email, "UTF-8");
//                data += "&" + URLEncoder.encode("roll_no", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(user.roll), "UTF-8");
//                data += "&" + URLEncoder.encode("department", "UTF-8") + "=" + URLEncoder.encode(user.dep, "UTF-8");
//                data += "&" + URLEncoder.encode("fingerprint", "UTF-8") + "=" + URLEncoder.encode(user.fingerprint , "UTF-8");
//                Log.d("tag", "stage 2");
//                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//                wr.write(data);
//                wr.flush();
//                wr.close();
                Log.d("TAG", con.getResponseCode() + "");
                JSONObject jsonObject1=new JSONObject(result);
                int uid=jsonObject1.getInt("uid");
                if(db.insertDataUser(uid,(user.isInstructor) ? "Instructor" : "Student", user.email, user.name, user.roll, user.dep, user.fingerprint))
                    Log.d("tag", "insert data in localdb okay");
                else
                    return false;

                return true;
            }
            catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean b) {
            progressDialog.dismiss();
            Log.d("tag", "stage exec");
            callback.done(b);
            super.onPostExecute(b);
        }
    }
    public class handleEmailFetch extends AsyncTask<Void,Void,User>{

        String email;
        RequestsCallback callback;
        public handleEmailFetch(String email,RequestsCallback callback){
            this.email=email;
            this.callback=callback;
        }

        @Override
        protected User doInBackground(Void... params) {
            try {
                HttpURLConnection con = (HttpURLConnection) (new URL(SERVER_ADDRESS + "getDetails.php")).openConnection();
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();

                String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");

                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.write(data);
                wr.flush();
                wr.close();

                InputStream inputStream = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream.close();
                String result = stringBuilder.toString();

                String s = "";
//                JSONArray jArray = new JSONArray(result);

                JSONObject json = new JSONObject(result);
                if (!json.getString("inst_stud").equalsIgnoreCase("Instructor")) {
                    return new User(json.getString("name"), json.getString("email"), json.getInt("roll_no"), json.getString("department"));
                } else {
                    return new User(json.getString("name"), json.getString("email"), json.getString("department"));
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
//                URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8");
//                data += "&" + URLEncoder.encode("age", "UTF-8") + "=" + URLEncoder.encode(age, "UTF-8");
//                data += "&" +
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            progressDialog.dismiss();

            callback.done(user);
            super.onPostExecute(user);
        }
    }

    public class handleAddStudent extends AsyncTask<Void,Void,Boolean>{
        int rollNumber;
        LocalDBHelper dbHelper;
        String course;
        RequestsCallback callback;
        SharedPreferences pref;
        public handleAddStudent(int rollNumber, String course,LocalDBHelper db, RequestsCallback callback,Context context) {
            this.rollNumber=rollNumber;
            this.course=course;
            this.callback=callback;
            this.dbHelper=db;
            this.pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            HttpURLConnection con = null;
            try {
//                if(dbHelper.insertAddStudent(rollNumber, course))
//                    Log.d("tag","insert in local db ok");
//                else
//                    return false;
//                con = (HttpURLConnection) (new URL(SERVER_ADDRESS + "registerStudentToCourse.php")).openConnection();
//                con.setRequestMethod("POST");
//                con.setDoInput(true);
//                con.setDoOutput(true);
//                con.connect();
                con = (HttpURLConnection) ( new URL("http://172.27.26.110/upasthiti/api/og/join")).openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
                Log.d("TAGn", pref.getString("session_name", null) + ":" + pref.getString("session_id", null) + "&&" + pref.getString("token", null));
                con.setRequestProperty(pref.getString("session_name", null), pref.getString("session_id", null));
                con.setRequestProperty("X-CSRF-Token", pref.getString("token", null));
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();

//                String data = URLEncoder.encode("courseno", "UTF-8") + "=" + URLEncoder.encode(course, "UTF-8");
//                data += "&" + URLEncoder.encode("roll_no", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(rollNumber), "UTF-8");
//                Log.d("tag", "stage 2");
//                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//                wr.write(data);
//                wr.flush();
//                wr.close();
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("course", dbHelper.getCourseNID(course));
                jsonObject.put("rollno", rollNumber);

                OutputStreamWriter printout=new OutputStreamWriter(con.getOutputStream());
                printout.write(jsonObject.toString());
                printout.flush();
                printout.close();

                InputStream inputStream = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream.close();
                String result = stringBuilder.toString();

                Log.d("TAG", con.getResponseCode() + "");
//                JSONObject jsonObject1=new JSONObject(result);
//                int nid=jsonObject1.getInt("nid");
                if(dbHelper.insertAddStudent(rollNumber, course))
                    Log.d("tag", "insert data in localdb okay");
                else
                    return false;

                return true;

            }catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
            progressDialog.dismiss();
            callback.done(b);
        }
    }


    public class handleFileFetch extends AsyncTask<Void,Void,String>{

        LocalDBHelper dataBase;
//        InputStream inputStream;
        BufferedReader reader;
        String course;
        RequestsCallback callback;
        SharedPreferences pref;
//        public handleFileFetch(LocalDBHelper db, InputStream inputStream,String course,RequestsCallback callback){
    public handleFileFetch(LocalDBHelper db, BufferedReader br,String course,RequestsCallback callback,Context context){
            this.dataBase=db;
//            this.reader=new BufferedReader(new InputStreamReader(inputStream));
//            this.inputStream=inputStream;
            this.callback=callback;
            this.reader=br;
            this.course=course;
            this.pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode

    }
        @Override
        protected String doInBackground(Void... params) {
            int total=0;
            String m="";
            int success=0;
            try{
                String csvLine;
                while ((csvLine = reader.readLine()) != null) {
                    StringTokenizer tokens = new StringTokenizer(csvLine, ",");
                    while(tokens.hasMoreTokens()){
                        int t=Integer.parseInt(tokens.nextToken());
//                        Start of http
                        HttpURLConnection con = (HttpURLConnection) ( new URL("http://172.27.26.110/upasthiti/api/og/join")).openConnection();
                        con.setRequestMethod("POST");
                        con.setRequestProperty("Content-Type", "application/json");
                        Log.d("TAGn", pref.getString("session_name", null) + ":" + pref.getString("session_id", null) + "&&" + pref.getString("token", null));
                        con.setRequestProperty(pref.getString("session_name", null), pref.getString("session_id", null));
                        con.setRequestProperty("X-CSRF-Token", pref.getString("token", null));
                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.connect();

//                String data = URLEncoder.encode("courseno", "UTF-8") + "=" + URLEncoder.encode(course, "UTF-8");
//                data += "&" + URLEncoder.encode("roll_no", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(rollNumber), "UTF-8");
//                Log.d("tag", "stage 2");
//                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//                wr.write(data);
//                wr.flush();
//                wr.close();
                        JSONObject jsonObject=new JSONObject();
                        jsonObject.put("course", dataBase.getCourseNID(course));
                        jsonObject.put("rollno", t);

                        OutputStreamWriter printout=new OutputStreamWriter(con.getOutputStream());
                        printout.write(jsonObject.toString());
                        printout.flush();
                        printout.close();

                        InputStream inputStream = con.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line + "\n");
                        }
                        inputStream.close();
                        String result = stringBuilder.toString();
                        con.disconnect();
//                        Stop of http
                        if(dataBase.insertAddStudent(t, this.course))
                            success++;
                        else
                        m+="1) "+t;
                        total++;
                    }
                }
//                if(flag)
//                    Toast.makeText(RegisterPerson.this, "Fetch from CSV File successful", Toast.LENGTH_SHORT).show();
//                else
//                    Toast.makeText(RegisterPerson.this,"Fetch from CSV failed",Toast.LENGTH_SHORT).show();
            }
            catch (Exception ex) {
                ex.printStackTrace();
                return ex.getMessage();
            }
            m = "There were " + total + " roll numbers found\n" + success + " numbers were entered succesfully\n"+
                        "The following roll numbers could not be entered for certain reasons:\n"+m;
            return m;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
//            add functionality to redirect to RegisterPerson
            callback.done(s);
            super.onPostExecute(s);
        }
    }
//
//    public class handleGetCourseList extends AsyncTask<Void,Void,Boolean>{
//        LocalDBHelper db;
//        String courseArray[];
//        public handleGetCourseList(LocalDBHelper db, RequestsCallback callback) {
//            this.db=db;
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//            HttpURLConnection con = null;
//            try {
////                if(db.insertDataCourse(course.courseId, course.courseName, course.instructor))
////                    Log.d("tag","insert in local db ok");
////                else
////                    return false;
//                con = (HttpURLConnection) (new URL(SERVER_ADDRESS + "getCourseList.php")).openConnection();
//                con.setRequestMethod("GET");
//                con.setDoInput(true);
//                con.setDoOutput(true);
//                con.connect();
//
////                String fingerprint;roll_no,department,fingerprint
////                Toast.makeText(RegisterPerson.this, "Fetch from CSV File successful", Toast.LENGTH_SHORT).show();
////                Log.d("tag", "stage 1");
////                String data = URLEncoder.encode("courseno", "UTF-8") + "=" + URLEncoder.encode(course.courseId, "UTF-8");
////                data += "&" + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(course.instructor, "UTF-8");
////                data += "&" + URLEncoder.encode("coursename", "UTF-8") + "=" + URLEncoder.encode(course.courseName, "UTF-8");
////                Log.d("tag", "stage 2");
////                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
////                wr.write(data);
////                wr.flush();
////                wr.close();
//                Log.d("TAG", con.getResponseCode() + "");
////                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
////                outputStreamWriter.write(data);
////                Log.d("tag", "stage 3");
////                outputStreamWriter.flush();
////                Log.d("tag", "stage 4");
////                outputStreamWriter.close();
//                InputStream inputStream = con.getInputStream();
//                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//                StringBuilder stringBuilder = new StringBuilder();
//                String line = null;
//                while ((line = reader.readLine()) != null) {
//                    stringBuilder.append(line + "\n");
//                }
//                inputStream.close();
//                String result = stringBuilder.toString();
//                JSONArray jArray = new JSONArray(result);
//
////                JSONObject json = new JSONObject(result);
////                if (!json.getString("inst_stud").equalsIgnoreCase("Instructor")) {
////                    return new User(json.getString("name"), json.getString("email"), json.getInt("roll_no"), json.getString("department"));
////                } else {
////                    return new User(json.getString("name"), json.getString("email"), json.getString("department"));
////                }
//                courseArray=new String[jArray.length()];
//                for(int i = 0; i < jArray.length(); i++)
//                {
//                    courseArray[i]= jArray.getJSONObject(i).getString("courseno");
//
//                }
//                return true;
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//            @Override
//        protected void onPostExecute(Boolean aVoid) {
//            super.onPostExecute(aVoid);
//        }
//
//    }
}
