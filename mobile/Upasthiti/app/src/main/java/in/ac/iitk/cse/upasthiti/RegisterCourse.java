package in.ac.iitk.cse.upasthiti;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.security.acl.LastOwnerException;
import java.util.EnumMap;


public class RegisterCourse extends ActionBarActivity implements View.OnClickListener {

    Button proceed;
    EditText courseNumber,courseName;
//    LinearLayout containerView;
    TextView addView;
    int num_email;
    SparseArray<EditText> EmailID=new SparseArray<EditText>();

    protected String strip(String string){
        String s=string.trim();
        s+="@";
        return string.substring(0,s.indexOf('@'));
    }
    protected boolean reject(String string){
        String s=string.trim();
        int specialCharCount=0,atCount=0;
        for (char c : string.toCharArray()) {
            if (!Character.isLetterOrDigit(c))
                specialCharCount++;
        }
        for (char c : string.toCharArray()) {
            if (c=='@'||c=='.'){ atCount++; }

        }
        return atCount!=specialCharCount;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_course);
        courseName=(EditText)findViewById(R.id.course_name);
        courseNumber=(EditText)findViewById(R.id.course_number);
        proceed=(Button)findViewById(R.id.button_proceed_c);
//        containerView=(LinearLayout)findViewById(R.id.parentView);
        addView=(TextView)findViewById(R.id.button_add_view);

        LinearLayout root = (LinearLayout) findViewById(R.id.root_regcourse);
        LinearLayout e;
        e= (LinearLayout) LayoutInflater.from(this).inflate(R.layout.edittextviews_id, root, false);
        root.addView(e);
        num_email=1;
        if(e.getChildAt(0) instanceof EditText) {
            EmailID.put(num_email,(EditText)e.getChildAt(0));
        }
        else {
            EmailID.put(num_email,(EditText)e.getChildAt(1));
        }

        proceed.setOnClickListener(this);
        addView.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Comment in any other suggestions

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_proceed_c:
                if(courseNumber.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(RegisterCourse.this,"Fill course number",Toast.LENGTH_SHORT).show();
                    break;
                }
                if(courseName.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterCourse.this,"Fill course name",Toast.LENGTH_SHORT).show();
                    break;
                }
                boolean flag_break=false;
                String emailIds="";
                for (int i = 0; i < num_email; i++) {
                    if(EmailID.get(i+1).getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(RegisterCourse.this, "Enter a valid email id", Toast.LENGTH_SHORT).show();
                        flag_break=true;
                        break;
                    }
                    String s=EmailID.get(i+1).getText().toString();
                    if(!reject(s))
                        emailIds+=strip(EmailID.get(i+1).getText().toString())+",";
                    else
                        Toast.makeText(RegisterCourse.this,EmailID.get(i+1).getText().toString()+" may not be an email-id", Toast.LENGTH_SHORT).show();
                }
                if(flag_break) break;
                Course course=new Course(courseNumber.getText().toString(),courseName.getText().toString(),emailIds.substring(0,emailIds.length()-1));
                regCourse(course);
                break;
            case R.id.button_add_view:
                LinearLayout root = (LinearLayout) findViewById(R.id.root_regcourse);
                LinearLayout editText;
                editText= (LinearLayout) LayoutInflater.from(this).inflate(R.layout.edittextviews_id,root,false);
                root.addView(editText);
                num_email++;
                if(editText.getChildAt(0) instanceof EditText) {
                    EmailID.put(num_email,(EditText)editText.getChildAt(0));
                }
                else {
                    EmailID.put(num_email,(EditText)editText.getChildAt(1));
                }
//                Toast.makeText(RegisterCourse.this,"Number of emails"+num_email,Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void deleteRow(View view) {
        if(num_email!=1) {
            LinearLayout root = (LinearLayout) findViewById(R.id.root_regcourse);
            root.removeView((View) view.getParent());
            num_email--;
        }else
        {
            EmailID.get(1).setText("");
        }
//        Toast.makeText(RegisterCourse.this,"Number of emails"+num_email,Toast.LENGTH_SHORT).show();
    }
    public void regCourse(Course course){
        AsyncRequests asyncRequests=new AsyncRequests(RegisterCourse.this,"Registering","Please wait while your course is registered");
        asyncRequests.sendCourseData(course,new LocalDBHelper(this),getApplicationContext(), new RequestsCallback() {
            @Override
            public void done(boolean b) {
                if(b)
                    Toast.makeText(RegisterCourse.this,"The course has been registered",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(RegisterCourse.this,"A problem was encountered",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void done(User e) {

            }

            @Override
            public void done(String s) {

            }

        });
    }
}
