package in.ac.iitk.cse.upasthiti;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGFingerInfo;

/**
 * Created by soumya on 26/6/15.
 */
/*public class FPHelper {
    //Converts image to grayscale (NEW)
    protected Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }
    //Converts image to binary (OLD)
    protected Bitmap toBinary(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public void captureFingerprint(byte[] fingerprintByte, byte[] fingerprintTemplate,int imageWidth,int imageHeight, JSGFPLib jsgfpLib){
        fingerprintByte = new byte[imageWidth*imageHeight];
        long result = jsgfpLib.GetImage(fingerprintByte);
//        DumpFile("capture.raw", buffer);
        Bitmap b = Bitmap.createBitmap(imageWidth,imageHeight, Bitmap.Config.ARGB_8888);
        b.setHasAlpha(false);
        int[] intbuffer = new int[imageWidth*imageHeight];
        for (int i=0; i<intbuffer.length; ++i)
            intbuffer[i] = (int) fingerprintByte[i];
        b.setPixels(intbuffer, 0, imageWidth, 0, 0, imageWidth,imageHeight);
//        fingerprint.setImageBitmap(this.toGrayscale(b));
        for (int i = 0; i < fingerprintTemplate.length; i++) {
            fingerprintTemplate[i]=0;
        }
        int[] quality=new int[1];
        jsgfpLib.GetImageQuality(imageWidth, imageHeight, fingerprintByte, quality);
        if(quality[0]<50)
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Fingerprint image is not of sufficient quality");
            dlgAlert.setTitle("Bad image");
            dlgAlert.setPositiveButton("CAPTURE AGAIN",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int whichButton){
//                            finish();
//                            return;
                            num_captured=0;
//                            startActivity(new Intent(FingerCapture.this, FingerCapture.class));

                        }
                    }
            );
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }
        result=jsgfpLib.CreateTemplate(new SGFingerInfo(),fingerprintByte, fingerprintTemplate);
    }
}
*/