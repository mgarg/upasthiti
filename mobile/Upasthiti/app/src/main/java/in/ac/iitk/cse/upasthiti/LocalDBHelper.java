package in.ac.iitk.cse.upasthiti;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Base64;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandipan on 21/6/15.
 */
public class LocalDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="data.db";
    public static final String USERTABLE_NAME="user_table";
    public static final String COURSETABLE_NAME="course_table";
    public static final String ADDSTUDENT_NAME="coursestudents_table";
//    USERTABLE_NAME
    public static final String uKEY_UID = "uid";
    public static final String uKEY_DESIG = "inst_stud";
    public static final String uKEY_EMAIL = "email";
    public static final String uKEY_NAME = "name";
    public static final String uKEY_ROLL = "roll_no";
    public static final String uKEY_DEPT = "department";
    public static final String uKEY_FP = "fingerprint";
//    COURSETABLE_NAME
    public static final String cKEY_NID = "nid";
    public static final String cKEY_COURSENAME = "coursename";
    public static final String cKEY_COURSEID   = "courseno";
    public static final String cKEY_INSTRUCTORS = "email";
//    ADDSTUDENT
    public static final String aKEY_COURSENAME = "coursename";
    public static final String aKEY_ROLL   = "roll_no";
//    FTS TABLE
    public static final String FTS_VIRTUAL_TABLE = "FTS";
    public static final String COL_ROLL = "roll_no";
    public static final String COL_NAME = "name";

    //Constructor

    public LocalDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + USERTABLE_NAME + " (" +uKEY_UID + " INTEGER, " + uKEY_DESIG + " TEXT, " + uKEY_EMAIL + " TEXT, " + uKEY_NAME + " TEXT, " + uKEY_ROLL + " INTEGER PRIMARY KEY, " + uKEY_DEPT + " TEXT, " + uKEY_FP + " TEXT);");
        db.execSQL("create table " + COURSETABLE_NAME + " (" +cKEY_NID + " INTEGER, " + cKEY_COURSENAME + " TEXT, " + cKEY_COURSEID + " TEXT, " + cKEY_INSTRUCTORS + " TEXT);");
        db.execSQL("create table " + ADDSTUDENT_NAME + " (" + aKEY_COURSENAME + " TEXT, " + aKEY_ROLL + " INTEGER);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + COURSETABLE_NAME);
        db.execSQL("drop table if exists " + USERTABLE_NAME);
        db.execSQL("drop table if exists " + ADDSTUDENT_NAME);
        onCreate(db);
    }

//    public void deleteVirtualTable(SQLiteDatabase db){
//        db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
//        onCreate(db);
//    }
    public boolean insertDataUser(int uid,String desig,String email,String name, int roll, String dept,String fp){
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(uKEY_UID,uid);
        contentValues.put(uKEY_DESIG,desig);
        contentValues.put(uKEY_EMAIL,email);
        contentValues.put(uKEY_NAME, name);
        contentValues.put(uKEY_ROLL, roll);
        contentValues.put(uKEY_DEPT, dept);
        contentValues.put(uKEY_FP, fp);

        return db.insert(USERTABLE_NAME, null, contentValues) != -1;
    }

    public boolean insertDataCourse(int nid,String courseid,String coursename,String emails){
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(cKEY_NID,nid);
        contentValues.put(cKEY_COURSEID,courseid);
        contentValues.put(cKEY_COURSENAME, coursename);
        contentValues.put(cKEY_INSTRUCTORS, emails);

        return db.insert(COURSETABLE_NAME, null, contentValues) != -1;
    }

    public boolean insertAddStudent(int roll,String coursename){
        SQLiteDatabase db=this.getWritableDatabase();

        Cursor cursor=getData(roll);
        if(cursor.getCount()==0)
            return false;
        ContentValues contentValues=new ContentValues();
        contentValues.put(aKEY_COURSENAME,coursename);
        contentValues.put(aKEY_ROLL,roll);

        return db.insert(ADDSTUDENT_NAME, null, contentValues) != -1;
    }

    public Cursor getData(){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.rawQuery("select * from "+USERTABLE_NAME+" ;",null);
    }
    public Cursor getData(int roll){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.rawQuery("select * from "+USERTABLE_NAME+" where "+uKEY_ROLL+" = "+roll+" ;",null);
    }
    public Cursor getData(String name){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.rawQuery("select * from "+USERTABLE_NAME+" where "+uKEY_NAME+" = '"+name+"' ;",null);
    }
    public Cursor getCourseData(){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.rawQuery("select * from "+COURSETABLE_NAME+" ;",null);
    }
    public int getCourseNID(String course){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor= db.rawQuery("select * from " + COURSETABLE_NAME + " where " + cKEY_COURSENAME + " = '" + course + "' ;", null);
        if(cursor.getCount()==0)
            return -1;
        int name=-1;
        while (cursor.moveToNext()){
            name=cursor.getInt(cursor.getColumnIndex(cKEY_NID));
        }
        cursor.close();
        return name;
    }
    public Cursor getStudentList(String course){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.rawQuery("select * from "+ADDSTUDENT_NAME+" where "+aKEY_COURSENAME+" = '"+course+"' ;",null);
    }
    public String nameOfRoll(int roll){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("select " + uKEY_NAME + " from " + USERTABLE_NAME + " where " + uKEY_ROLL + " = " + roll + " ;", null);
        if(cursor.getCount()==0)
            return null;
        String name="";
        while (cursor.moveToNext()){
            name=cursor.getString(cursor.getColumnIndex(uKEY_NAME));
        }
        cursor.close();
        return name;
    }
    public List<User> readBlob(String courseNumber){
        SQLiteDatabase db = this.getReadableDatabase();
        List<User> list = new ArrayList<User>();
        // TODOIMP
        String joinquery = "SELECT * FROM "+ADDSTUDENT_NAME+" a INNER JOIN "+USERTABLE_NAME+" b ON a."+aKEY_ROLL+"=b."+uKEY_ROLL+" WHERE a."+aKEY_COURSENAME+"=?";
        Cursor c = db.rawQuery(joinquery, new String[]{courseNumber});
        Log.d("query", "join query returns");


	/*	String selectQuery = "SELECT * FROM StudentDetails WHERE CourseNumber=?";
		Cursor c = db.rawQuery(selectQuery, new String[]{courseNumber});
		Log.d("query", "query returns");
	*/

        String fingerPrint = "";
        if (c.moveToFirst()) {
            do {
                fingerPrint = c.getString(c.getColumnIndex(uKEY_FP));
                String name = c.getString(c.getColumnIndex(uKEY_NAME));
                int roll = c.getInt(c.getColumnIndex(uKEY_ROLL));
                String dep=c.getString(c.getColumnIndex(uKEY_DEPT));
                String email=c.getString(c.getColumnIndex(uKEY_EMAIL));
                User user = new User(name,email,roll,dep);
                user.fingerprint=fingerPrint;
                user.BLOBfingerprint= Base64.decode(fingerPrint,Base64.DEFAULT);
                list.add(user);
            } while (c.moveToNext());
        }
        return list;

    }

//    public long addWord(int roll, String name) {
//        SQLiteDatabase db=this.getWritableDatabase();
//        ContentValues initialValues = new ContentValues();
//        initialValues.put(COL_ROLL, roll);
//        initialValues.put(COL_NAME, name);
//
//        return db.insert(FTS_VIRTUAL_TABLE, null, initialValues);
//        return 1;
//    }

//    public Cursor getWordMatches(String query, String[] columns) {
//        String selection = COL_ROLL + " MATCH ?";
//        String[] selectionArgs = new String[] {query+"*"};
//
//        return query(selection, selectionArgs, columns);
//    }
//
//    private Cursor query(String selection, String[] selectionArgs, String[] columns) {
//        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
//        builder.setTables(FTS_VIRTUAL_TABLE);
//
//        Cursor cursor = builder.query(this.getReadableDatabase(),
//                columns, selection, selectionArgs, null, null, null);
//
//        if (cursor == null) {
//            return null;
//        } else if (!cursor.moveToFirst()) {
//            cursor.close();
//            return null;
//        }
//        return cursor;
//    }
}
