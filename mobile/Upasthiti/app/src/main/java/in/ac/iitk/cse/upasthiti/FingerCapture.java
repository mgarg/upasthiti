package in.ac.iitk.cse.upasthiti;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.FeatureInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.util.Scanner;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;


public class FingerCapture extends ActionBarActivity implements View.OnClickListener {

    // All variables required by SDK and activity_finger_capture
    Button capture;
    JSGFPLib jsgfpLib;
    ImageView fingerprint;
    protected byte[] fingerprintByte1;
    protected byte[] fingerprintTemplate1;
    protected byte[] fingerprintByte2;
    protected byte[] fingerprintTemplate2;
    protected IntentFilter filter;
    protected PendingIntent permissionIntent;
    protected int imageWidth;
    protected int imageHeight;
    protected int[] maxTemplateSize;
    protected int[] grayBuffer;
    protected Bitmap grayBitmap;
    protected byte num_captured;
    protected User user;
    //Requesting permissions and establishing connection with device
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            //DEBUG Log.d(TAG,"Enter mUsbReceiver.onReceive()");
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){
                            //DEBUG Log.d(TAG, "Vendor ID : " + device.getVendorId() + "\n");
                            //DEBUG Log.d(TAG, "Product ID: " + device.getProductId() + "\n");
                            //debugMessage("Vendor ID : " + device.getVendorId() + "\n");
                            //debugMessage("Product ID: " + device.getProductId() + "\n");
                            Toast.makeText(FingerCapture.this,"mUsbReceiver.onReceive() Device found",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(FingerCapture.this, "mUsbReceiver.onReceive() Device is null", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else Toast.makeText(FingerCapture.this,"mUsbReceiver.onReceive() permission denied for device " + device,Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
//    Function assignment block ends
    // Enables capture button
    public void EnableControls(){
    this.capture.setClickable(true);
//    this.mCapture.setTextColor(getResources().getColor(android.R.color.white));
//    this.mButtonRegister.setClickable(true);
//    this.mButtonRegister.setTextColor(getResources().getColor(android.R.color.white));
//    this.mButtonMatch.setClickable(true);
//    this.mButtonMatch.setTextColor(getResources().getColor(android.R.color.white));
}
    // Disables capture button
    public void DisableControls(){
        this.capture.setClickable(false);
//        this.mCapture.setTextColor(getResources().getColor(android.R.color.black));
//        this.mButtonRegister.setClickable(false);
//        this.mButtonRegister.setTextColor(getResources().getColor(android.R.color.black));
//        this.mButtonMatch.setClickable(false);
//        this.mButtonMatch.setTextColor(getResources().getColor(android.R.color.black));
    }

    //Converts image to grayscale (NEW)
    protected Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }

    //Converts image to binary (OLD)
    protected Bitmap toBinary(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public void captureFingerprint(byte[] fingerprintByte, final byte[] fingerprintTemplate){
        fingerprintByte = new byte[imageWidth*imageHeight];
        long result = jsgfpLib.GetImage(fingerprintByte);
//        DumpFile("capture.raw", buffer);
        Bitmap b = Bitmap.createBitmap(imageWidth,imageHeight, Bitmap.Config.ARGB_8888);
        b.setHasAlpha(false);
        int[] intbuffer = new int[imageWidth*imageHeight];
        for (int i=0; i<intbuffer.length; ++i)
            intbuffer[i] = (int) fingerprintByte[i];
        b.setPixels(intbuffer, 0, imageWidth, 0, 0, imageWidth,imageHeight);
        fingerprint.setImageBitmap(this.toGrayscale(b));
        for (int i = 0; i < fingerprintTemplate.length; i++) {
            fingerprintTemplate[i]=0;
        }
        int[] quality=new int[1];
        jsgfpLib.GetImageQuality(imageWidth, imageHeight, fingerprintByte, quality);
        if(quality[0]<50)
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Fingerprint image is not of sufficient quality");
            dlgAlert.setTitle("Bad image");
            dlgAlert.setPositiveButton("CAPTURE AGAIN",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int whichButton){
//                            finish();
//                            return;
                            num_captured=0;
//                            startActivity(new Intent(FingerCapture.this, FingerCapture.class));

                        }
                    }
            );
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }
        result=jsgfpLib.CreateTemplate(new SGFingerInfo(),fingerprintByte, fingerprintTemplate);
    }
    // Sets initial view of fingerprint GRAY
    protected void setGrayBitmap(){
        grayBuffer = new int[JSGFPLib.MAX_IMAGE_WIDTH_ALL_DEVICES*JSGFPLib.MAX_IMAGE_HEIGHT_ALL_DEVICES];
        for (int i=0; i<grayBuffer.length; ++i)
            grayBuffer[i] = Color.WHITE;
        grayBitmap = Bitmap.createBitmap(JSGFPLib.MAX_IMAGE_WIDTH_ALL_DEVICES, JSGFPLib.MAX_IMAGE_HEIGHT_ALL_DEVICES, Bitmap.Config.ARGB_8888);
        grayBitmap.setPixels(grayBuffer, 0, JSGFPLib.MAX_IMAGE_WIDTH_ALL_DEVICES, 0, 0, JSGFPLib.MAX_IMAGE_WIDTH_ALL_DEVICES, JSGFPLib.MAX_IMAGE_HEIGHT_ALL_DEVICES);
        fingerprint.setImageBitmap(grayBitmap);
    }
    // Connects as required to USB
    protected void setConnectionUSB(){
        //USB Permissions
        permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, filter);
        jsgfpLib = new JSGFPLib((UsbManager)getSystemService(Context.USB_SERVICE));
//        this.mToggleButtonSmartCapture.toggle();
//
//
//        debugMessage("jnisgfplib version: " + sgfplib.Version() + "\n");
//        mLed = false;
//        autoOn = new SGAutoOnEventNotifier(sgfplib, this);
//        mAutoOnEnabled = false;
    }
    public User getUser(){
        SharedPreferences sharedPreferences=getSharedPreferences("User",Context.MODE_PRIVATE);
        String userName=sharedPreferences.getString("userName","");
        String userEmail=sharedPreferences.getString("userEmail","");
        String userDep=sharedPreferences.getString("userDep","");
        int userRoll=sharedPreferences.getInt("userRoll",-1);
        boolean isInstructor=sharedPreferences.getBoolean("isInstructor",true);
        Toast.makeText(FingerCapture.this,"User : "+userName,Toast.LENGTH_SHORT).show();

        if(isInstructor){
            return new User(userName,userEmail,userDep);
        }
        else
        {
            return new User(userName,userEmail,userRoll,userDep);
        }

    }
    public void userPostData(){

//       user.fingerprint = Base64.encodeToString(fingerprintTemplate2, Base64.DEFAULT);
        user.fingerprint = "TESTFINGERPRINT";
        final AsyncRequests asyncRequests = new AsyncRequests(this, "Please wait", "Sending data to server");
        asyncRequests.sendUserData(user, new LocalDBHelper(this),getApplicationContext(), new RequestsCallback() {
            @Override
            public void done(boolean b) {
                if(b) {
                    Toast.makeText(FingerCapture.this, "Everything went alright", Toast.LENGTH_SHORT).show();
                    TextView editText = (TextView) findViewById(R.id.hook_fptext);
                    editText.setText("You are good to go!");
                    editText.setBackgroundColor(Color.parseColor("#388e3c"));
                }
                else{
                    Toast.makeText(FingerCapture.this, "An error was encountered", Toast.LENGTH_SHORT).show();
                    TextView editText = (TextView) findViewById(R.id.hook_fptext);
                    editText.setText("We might have had an error");
                }
            }


            @Override
            public void done(User e) {

            }

            @Override
            public void done(String s) {

            }

        });
    }
    // onCreate() starts here
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_capture);
//        if (android.os.Build.VERSION.SDK_INT > 9) {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }
        user=getUser();
        capture=(Button)findViewById(R.id.button_capture);
        fingerprint=(ImageView)findViewById(R.id.fingerprint_view);
        setGrayBitmap();
        maxTemplateSize=new int[1];
        setConnectionUSB();

        capture.setOnClickListener(this);
    }
    // may ignore
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_finger_capture, menu);
        return true;
    }
    // may ignore
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Setting onPause of current activity
    @Override
    public void onPause() {
        EnableControls();
        jsgfpLib.CloseDevice();
        unregisterReceiver(mUsbReceiver);
        fingerprintByte1 = null;
        fingerprintTemplate1 = null;
        fingerprintByte2 = null;
        fingerprintTemplate2 = null;
        fingerprint.setImageBitmap(grayBitmap);
        super.onPause();
    }

    // Whenever activity starts(OR resumes)
    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(mUsbReceiver, filter);
        long error = jsgfpLib.Init(SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE){
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)
                dlgAlert.setMessage("The attached fingerprint device is not supported by the device");
            else
                dlgAlert.setMessage("Fingerprint device initialization failed!");
            dlgAlert.setTitle("SecuGen Fingerprint SDK");
            dlgAlert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int whichButton){
//                            finish();
                            return;
                        }
                    }
            );
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }
        else {
            UsbDevice usbDevice = jsgfpLib.GetUsbDevice();
            if (usbDevice == null){
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int whichButton){
//                                finish();
                                return;
                            }
                        }
                );
                dlgAlert.setCancelable(false);
                dlgAlert.create().show();
            }
            else {
                jsgfpLib.GetUsbManager().requestPermission(usbDevice, permissionIntent);
                error = jsgfpLib.OpenDevice(0);
                SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                error = jsgfpLib.GetDeviceInfo(deviceInfo);
                imageWidth = deviceInfo.imageWidth;
                imageHeight= deviceInfo.imageHeight;
                jsgfpLib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                jsgfpLib.GetMaxTemplateSize(maxTemplateSize);
                fingerprintTemplate1 = new byte[maxTemplateSize[0]];
                fingerprintTemplate2 = new byte[maxTemplateSize[0]];
                jsgfpLib.WriteData((byte)5, (byte)1);
            }
        }
    }

    // Whenever the activity is destroyed(EG: BY PRESSING BACK KEY)
    @Override
    public void onDestroy() {
        jsgfpLib.CloseDevice();
        fingerprintByte1 = null;
        fingerprintTemplate1 = null;
        fingerprintByte2 = null;
        fingerprintTemplate2 = null;
        jsgfpLib.Close();
        super.onDestroy();
    }
    //To-Do: POST functionality
    //To-Do: Custom values for Image-quality to be set
    //Comment in any other suggestions
    @Override
    public void onClick(View v) {
        if (v == capture) {
            userPostData();
//            num_captured++;
//            boolean flag_local = true;
//            try {
//                if(num_captured == 1) {
//                    captureFingerprint(fingerprintByte1, fingerprintTemplate1);
//                    Toast.makeText(FingerCapture.this,"We need a second sample, please repeat",Toast.LENGTH_SHORT).show();
//                }else if(num_captured==2)
//                {
//                    captureFingerprint(fingerprintByte2,fingerprintTemplate2);
//                    boolean[] matched=new boolean[1];
//                    long result=jsgfpLib.MatchTemplate(fingerprintTemplate1,fingerprintTemplate2,SGFDxSecurityLevel.SL_ABOVE_NORMAL,matched);
//                    if(matched[0]){
//                        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//                        dlgAlert.setMessage("Fingerprints match");
//                        dlgAlert.setTitle("SUCCESS");
//                        dlgAlert.setPositiveButton("PROCEED",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int whichButton) {
////                                    TO Connect
//                                        user=getUser();
//                                        try {
//                                            userPostData();
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Toast.makeText(FingerCapture.this,"Error",Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                }
//                        );
//                        dlgAlert.setNegativeButton("TRY AGAIN",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,int whichButton){
////                                startActivity(new Intent(FingerCapture.this, MainActivity.class));
////                                finish();
//                            }
//                        });
//                        dlgAlert.setCancelable(false);
//                        dlgAlert.create().show();
//                    }
//                    num_captured=0;
//                }
//
//            }catch (Exception e) {
////                flag_local = false;
//                num_captured=0;
//                final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//                dlgAlert.setMessage("Error message: " + e.getMessage());
//                dlgAlert.setTitle("Something went wrong");
//                dlgAlert.setPositiveButton("TRY AGAIN",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                            }
//                        }
//                );
//                dlgAlert.setNegativeButton("TAKE ME BACK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        startActivity(new Intent(FingerCapture.this, MainActivity.class));
//                        finish();
//                    }
//                });
//                dlgAlert.setCancelable(false);
//                dlgAlert.create().show();
//            }
//            END here
//            }finally {
//                int[] quality=new int[1];
//                jsgfpLib.GetImageQuality(imageWidth, imageHeight, fingerprintByte, quality);
//                if(flag_local) {
//                    if(quality[0]>50) {
//                        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//                        dlgAlert.setMessage("Do you want to view the image?");
//                        dlgAlert.setTitle("Capture successful");
//                        dlgAlert.setPositiveButton("YES",null);
//                        dlgAlert.setNegativeButton("NO, PROCEED", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                Toast.makeText(FingerCapture.this,"Now to send all data to server",Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                        dlgAlert.setCancelable(false);
//                        dlgAlert.create().show();
//                    }
//                    else
//                    {
//                        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//                        dlgAlert.setMessage("You will have to capture the fingerprint again");
//                        dlgAlert.setTitle("Bad capture quality");
//                        dlgAlert.setPositiveButton("YES",null);
//                        dlgAlert.setNegativeButton("NO, PROCEED", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                startActivity(new Intent(FingerCapture.this, MainActivity.class));
//                                finish();
//                            }
//                        });
//                        dlgAlert.setCancelable(false);
//                        dlgAlert.create().show();
//                    }
//                }
//            }
        }
    }
}
