package in.ac.iitk.cse.upasthiti;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import SecuGen.FDxSDKPro.JSGFPLib;
import SecuGen.FDxSDKPro.SGAutoOnEventNotifier;
import SecuGen.FDxSDKPro.SGFDxDeviceName;
import SecuGen.FDxSDKPro.SGFDxErrorCode;
import SecuGen.FDxSDKPro.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.SGFingerInfo;
import SecuGen.FDxSDKPro.SGFingerPresentEvent;


public class TakeAttendance extends Activity implements SGFingerPresentEvent {

    LocalDBHelper dbHelper;
    String courseID;
    ImageView fingerprint;
    TextView name,roll;
    TextView logMessages;
    TextView present,total;
    JSGFPLib jsgfpLib;
    IntentFilter filter;
    int presentNum=0;
    SGAutoOnEventNotifier autoOn;
    byte[] fingerprintTemplate;
    int[] maxTemplateSize;
    private List<User> allUsers;
    private List<byte[]> capturedFingerprints;
    int imageWidth,imageHeight;
    PendingIntent pendingIntent;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private static final String TAG="Attendance";
    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG,"Enter usbReceiver.onReceive()");
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){
                            Log.d(TAG, "Vendor ID : " + device.getVendorId() + "\n");
                            Log.d(TAG, "Product ID: " + device.getProductId() + "\n");

                        }
                        else
                            Log.e(TAG, "usbReceiver.onReceive() Device is null");
                    }
                    else Log.e(TAG, "usbReceiver.onReceive() permission denied for device " + device);
                }
            }
        }
    };

    //This message handler is used to access local resources not
    //accessible by SGFingerPresentCallback() because it is called by
    //a separate thread.
    public Handler fingerDetectedHandler = new Handler(){
        // @Override
        public void handleMessage(Message msg) {
            //Handle the message
            CaptureFingerPrint();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_attendance);

        Bundle bundle = getIntent().getExtras();
        courseID = bundle.getString("courseID");

        dbHelper=new LocalDBHelper(this);

        View decorView = getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }


        fingerprint=(ImageView)findViewById(R.id.imageView);
        name=(TextView)findViewById(R.id.NAME);
        roll=(TextView)findViewById(R.id.ROLL);
        logMessages=(TextView)findViewById(R.id.logMessages);
        present=(TextView)findViewById(R.id.PRESENT);
        total=(TextView)findViewById(R.id.TOTAL);

//        Sector BREAK
        maxTemplateSize=new int[1];
        pendingIntent= PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(usbReceiver, filter);
        jsgfpLib=new JSGFPLib((UsbManager)getSystemService(Context.USB_SERVICE));

//        Preparing the lists
        allUsers=dbHelper.readBlob(courseID);
        total.setText(allUsers.size()+"");
        capturedFingerprints=new ArrayList<byte[]>();

        Log.d(TAG,"jnisgfplib version: " + jsgfpLib.Version() + "\n");
        autoOn = new SGAutoOnEventNotifier (jsgfpLib, this);
        autoOn.start();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");
        autoOn.stop();
        jsgfpLib.CloseDevice();
        unregisterReceiver(usbReceiver);
        super.onPause();
    }

        @Override
        public void onWindowFocusChanged(boolean hasFocus) {
            super.onWindowFocusChanged(hasFocus);
            View decorView = getWindow().getDecorView();
// Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
            autoOn.start();
        }

        @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        registerReceiver(usbReceiver, filter);
        long error=jsgfpLib.Init(SGFDxDeviceName.SG_DEV_AUTO);
        if (error != SGFDxErrorCode.SGFDX_ERROR_NONE){
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            if (error == SGFDxErrorCode.SGFDX_ERROR_DEVICE_NOT_FOUND)
                dlgAlert.setMessage("The attached fingerprint device is not supported on Android");
            else
                dlgAlert.setMessage("Fingerprint device initialization failed!");
            dlgAlert.setTitle("SecuGen Fingerprint SDK");
            dlgAlert.setPositiveButton("OK",null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
        else {
            UsbDevice usbDevice = jsgfpLib.GetUsbDevice();
            if (usbDevice == null){
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                dlgAlert.setMessage("SDU04P or SDU03P fingerprint sensor not found!");
                dlgAlert.setTitle("SecuGen Fingerprint SDK");
                dlgAlert.setPositiveButton("OK",null);
                dlgAlert.setCancelable(false);
                dlgAlert.create().show();
            }
            else {
                jsgfpLib.GetUsbManager().requestPermission(usbDevice, pendingIntent);
                error = jsgfpLib.OpenDevice(0);
                Log.d(TAG,"OpenDevice() ret: " + error + "\n");
                SecuGen.FDxSDKPro.SGDeviceInfoParam deviceInfo = new SecuGen.FDxSDKPro.SGDeviceInfoParam();
                error = jsgfpLib.GetDeviceInfo(deviceInfo);
                Log.d(TAG,"GetDeviceInfo() ret: " + error + "\n");
                imageWidth = deviceInfo.imageWidth;
                imageHeight= deviceInfo.imageHeight;
                jsgfpLib.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_SG400);
                jsgfpLib.GetMaxTemplateSize(maxTemplateSize);
                Log.d(TAG,"TEMPLATE_FORMAT_SG400 SIZE: " + maxTemplateSize[0] + "\n");
                fingerprintTemplate = new byte[maxTemplateSize[0]];
//                mVerifyTemplate = new byte[mMaxTemplateSize[0]];
//                boolean smartCaptureEnabled = this.mToggleButtonSmartCapture.isChecked();
//                if (smartCaptureEnabled)

//                SMART ENABLED BY DEFAULT
                jsgfpLib.WriteData((byte)5, (byte)1);
//                else
//                    jsgfpLib.WriteData((byte)5, (byte)0);
//                if (mAutoOnEnabled){

//                AUTOON ENABLED
                autoOn.start();
//                    DisableControls();
                }
                //Thread thread = new Thread(this);
                //thread.start();
            }
            View decorView = getWindow().getDecorView();
// Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        jsgfpLib.CloseDevice();
        jsgfpLib.Close();
        super.onDestroy();
    }

    //Converts image to grayscale (NEW)
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int y=0; y< height; ++y) {
            for (int x=0; x< width; ++x){
                int color = bmpOriginal.getPixel(x, y);
                int r = (color >> 16) & 0xFF;
                int g = (color >> 8) & 0xFF;
                int b = color & 0xFF;
                int gray = (r+g+b)/3;
                color = Color.rgb(gray, gray, gray);
                //color = Color.rgb(r/3, g/3, b/3);
                bmpGrayscale.setPixel(x, y, color);
            }
        }
        return bmpGrayscale;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_take_attendance, menu);
        return true;
    }

    public void SGFingerPresentCallback (){
        autoOn.stop();
        fingerDetectedHandler.sendMessage(new Message());
    }

    public void CaptureFingerPrint(){
//        long dwTimeStart = 0, dwTimeEnd = 0, dwTimeElapsed = 0;
        byte[] buffer = new byte[imageWidth*imageHeight];
//        dwTimeStart = System.currentTimeMillis();
        long result = jsgfpLib.GetImage(buffer);
//        DumpFile("capture.raw", buffer);
//        dwTimeEnd = System.currentTimeMillis();
//        dwTimeElapsed = dwTimeEnd-dwTimeStart;
        Log.d(TAG, "getImage() ret:" + result + "\n");
        autoOn.stop();
        Bitmap b = Bitmap.createBitmap(imageWidth,imageHeight, Bitmap.Config.ARGB_8888);
        b.setHasAlpha(false);
        int[] intbuffer = new int[imageWidth*imageHeight];
        for (int i=0; i<intbuffer.length; ++i)
            intbuffer[i] = (int) buffer[i];
        b.setPixels(intbuffer, 0, imageWidth, 0, 0, imageWidth, imageHeight);
        fingerprint.setImageBitmap(this.toGrayscale(b));

        for (int i = 0; i < fingerprintTemplate.length; i++) {
            fingerprintTemplate[i]=0;
        }
        int[] quality=new int[1];
        jsgfpLib.GetImageQuality(imageWidth, imageHeight, buffer, quality);
        if(quality[0]<40)
        Toast.makeText(this,"Bad capture quality of fingerprint",Toast.LENGTH_SHORT).show();

        result=jsgfpLib.CreateTemplate(new SGFingerInfo(),buffer, fingerprintTemplate);

        capturedFingerprints.add(fingerprintTemplate);

        matchTemplates();
        capturedFingerprints.clear();
        autoOn.start();

    }

    private void matchTemplates() {
        boolean[] matched=new boolean[1];
        boolean found=false;
        for (int i = 0; i < capturedFingerprints.size(); i++) {
            for (int j = 0; j < allUsers.size(); j++) {
                long result=jsgfpLib.MatchTemplate(capturedFingerprints.get(i),allUsers.get(j).BLOBfingerprint, SGFDxSecurityLevel.SL_NORMAL,matched);
                if(matched[0]){
                    found=true;
                    logMessages.setText("YOUR ATTENDANCE HAS BEEN TAKEN");
                    logMessages.setTextColor(Color.parseColor("#fafafa"));
                    logMessages.setBackgroundColor(Color.parseColor("#4caf50"));
                    name.setText(allUsers.get(j).name);
                    roll.setText(allUsers.get(j).roll+"");
                    present.setText(++presentNum+"");
                    break;
                }
                if(found)
                    break;
            }
            if(!found){
                logMessages.setText("FAILED");
                logMessages.setTextColor(Color.parseColor("#fafafa"));
                logMessages.setBackgroundColor(Color.parseColor("#ef5350"));
                name.setText("");
                roll.setText("");
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
