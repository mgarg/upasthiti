package in.ac.iitk.cse.upasthiti;

/**
 * Created by soumya on 22/6/15.
 */
public class User {
    String name;
    String email;
    int roll;
    boolean isInstructor;
    String dep;
    String fingerprint;
    byte[] BLOBfingerprint;
//    For Students
    public User(String name,String email,int roll,String dep){
        this.name=name;
        this.email=email;
        this.roll=roll;
        this.isInstructor=false;
        this.dep=dep;
        this.fingerprint="";
        this.BLOBfingerprint="".getBytes();
    }
//    For Instructors
    public User(String name,String email,String dep){
        this.name=name;
        this.email=email;
        this.roll=-1;
        this.isInstructor=true;
        this.dep=dep;
        this.fingerprint="";
        this.BLOBfingerprint="".getBytes();
    }


}
