package in.ac.iitk.cse.upasthiti;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog progressDialog;
    //Two buttons in corresponding activity
    Button bRegisterCourse,bRegisterPerson,bAddstudent,bTakeAttendance;
    Spinner courseSpinner;
    ArrayAdapter<CharSequence> adapter;

    public void populateSpinner(){
        progressDialog=new ProgressDialog(this);
//        progressDialog.setMessage("");
        progressDialog.setTitle("Please wait");
        progressDialog.setCancelable(true);
        progressDialog.show();
        new handleGetCourseList().execute();

        progressDialog.dismiss();
//        LocalDBHelper dbHelper=new LocalDBHelper(this);
//        Cursor cursor=dbHelper.getCourseData();
//        if(cursor.getCount()==0)
//        {
//            Toast.makeText(this,"No course registered yet",Toast.LENGTH_SHORT).show();
//            return;
//        }
//        int i=0;
//        courseArray=new String[cursor.getCount()];
//        while (cursor.moveToNext()){
//            courseArray[i++]=cursor.getString(1);
//        }


    }

    @Override
    protected void onResume() {
//        populateSpinner();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bRegisterCourse=(Button)findViewById(R.id.register_course);
        bRegisterPerson=(Button)findViewById(R.id.register_person);
        bAddstudent=(Button)findViewById(R.id.add_student);
        bTakeAttendance=(Button)findViewById(R.id.take_attendance);
        courseSpinner=(Spinner)findViewById(R.id.spinnerCourse);
        populateSpinner();
        bRegisterPerson.setOnClickListener(this);
        bRegisterCourse.setOnClickListener(this);
        bAddstudent.setOnClickListener(this);
        bTakeAttendance.setOnClickListener(this);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //onclick will send each Intent to respective activity
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register_person:
                startActivity(new Intent(this,RegisterPerson.class));
                break;
            case R.id.register_course:
                startActivity(new Intent(this,RegisterCourse.class));
                break;
            case R.id.add_student:
                String courseID = courseSpinner.getSelectedItem().toString();
                Intent intent = new Intent(MainActivity.this, AddStudent.class);
                intent.putExtra("courseID", courseID);
                startActivity(intent);
                break;
            case R.id.take_attendance:
                courseID = courseSpinner.getSelectedItem().toString();
                intent = new Intent(MainActivity.this, TakeAttendance.class);
                intent.putExtra("courseID", courseID);
                startActivity(intent);
                break;
            default:
                Toast.makeText(this,"Something went wrong",Toast.LENGTH_LONG).show();
        }
    }
    private class handleGetCourseList extends AsyncTask<Void,Void,String>{
        String courseArray[];
        @Override
        protected String doInBackground(Void... params) {
            String result="";
            try {
                HttpURLConnection con=(HttpURLConnection) ( new URL("http://172.27.26.110/upasthiti/api/user/login.json")).openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();

                JSONObject jsonObject=new JSONObject();
                jsonObject.put("username", "superuser");
                jsonObject.put("password", "Superuser@UP");
                OutputStreamWriter printout=new OutputStreamWriter(con.getOutputStream());
                printout.write(jsonObject.toString());
                printout.flush();
                printout.close();

                InputStream inputStream = con.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream.close();
                result = stringBuilder.toString();
                Log.d("TAGserver", result + "\n");

//                JSONArray jArray = new JSONArray(result);
//                if(jArray.length()<1) {
//                    Toast.makeText(MainActivity.this,"No course registered yet",Toast.LENGTH_SHORT).show();
//                    return null;
//                }
                Log.d("TAG", con.getResponseCode() + "");

                JSONObject json = new JSONObject(result);
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("session_name",json.getString("session_name"));
                editor.putString("session_id",json.getString("sessid"));
                editor.putString("token", json.getString("token"));
                editor.apply();
                Log.d("TAGn", pref.getString("session_name", null) + ":" + pref.getString("session_id", null) + "&&" + pref.getString("token", null));

                HttpURLConnection con2 = (HttpURLConnection) (new URL("http://172.27.26.110/upasthiti/api/courses")).openConnection();
                con2.setRequestMethod("GET");
                con2.setRequestProperty("Content-Type", "application/json");
                con2.setRequestProperty("Cookie",pref.getString("session_name", null)+"="+pref.getString("session_id", null));
                con2.setRequestProperty("X-CSRF-Token", json.getString("token"));
                con2.setDoInput(true);
                con2.setDoOutput(false);
                con2.connect();

                Log.d("TAGn", con2.getResponseCode() + "");
                InputStream inputStream2 = con2.getInputStream();
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(inputStream2));
                stringBuilder = null;
                stringBuilder = new StringBuilder();
                line = null;
                while ((line = reader2.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                inputStream2.close();
                result = stringBuilder.toString();
                JSONArray jArray = new JSONArray(result);
                courseArray=new String[jArray.length()];
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject item=jArray.getJSONObject(i);
                    courseArray[i]=item.getString("coursename");
                }


            } catch (IOException e) {
                Log.d("TAG", "IOE");
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                Log.d("TAG", "JSONE");
                e.printStackTrace();
                return null;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if(aVoid!=null) {
                adapter = new ArrayAdapter<CharSequence>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, courseArray);
                courseSpinner.setAdapter(adapter);
            }
            else
            {
                Toast.makeText(MainActivity.this,"Could not connect to server",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
