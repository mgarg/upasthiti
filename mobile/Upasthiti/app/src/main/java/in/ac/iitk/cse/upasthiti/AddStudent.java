package in.ac.iitk.cse.upasthiti;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.nio.Buffer;
import java.sql.BatchUpdateException;


public class AddStudent extends ActionBarActivity implements View.OnClickListener{

//    TabHost tabHost;
    EditText rollNumber;
    Button add,importData,viewData;
    String courseID;
    TextView textView,numcount;
    TextView errorLog;
    String m_ChosenDir;
    boolean m_NewFolderEnabled;
//    TableLayout headerTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        Bundle bundle = getIntent().getExtras();
        courseID = bundle.getString("courseID");
        errorLog=(TextView)findViewById(R.id.errorImportLog);
//        tabHost=(TabHost)findViewById(R.id.tabHost);
//        tabHost.setup();

//        TabHost.TabSpec tabSpec=tabHost.newTabSpec("ADD");
//        tabSpec.setContent(R.id.tabAdd);
//        tabSpec.setIndicator("Add a student");
//        tabHost.addTab(tabSpec);
        textView=(TextView)findViewById(R.id.textView);
        textView.setText("Number of students in "+courseID);
        numcount=(TextView)findViewById(R.id.numCount);
        textView.setBackgroundColor(Color.parseColor("#4a148c"));
        textView.setTextColor(Color.WHITE);
        numcount.setBackgroundColor(Color.parseColor("#ede7f6"));
        Cursor cursor= new LocalDBHelper(this).getStudentList(courseID);
        numcount.setText(cursor.getCount() + "");
        rollNumber=(EditText)findViewById(R.id.roll_student);
        add=(Button)findViewById(R.id.button_add_roll);
        viewData=(Button)findViewById(R.id.button_view);
        importData=(Button)findViewById(R.id.button_import);

//        tabSpec=tabHost.newTabSpec("VIEW");
//        tabSpec.setContent(R.id.tabView);
//        tabSpec.setIndicator("View enrolled students");
//        tabHost.addTab(tabSpec);

//        headerTable=(TableLayout)findViewById(R.id.header_table);
//        headerTable = addRowToTable(headerTable, "Hour", "Avg");
        add.setOnClickListener(this);
        viewData.setOnClickListener(this);
        importData.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_add_roll:
                if(rollNumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(AddStudent.this, "Enter your roll number", Toast.LENGTH_SHORT).show();
                    break;
                }
                AsyncRequests asyncRequests=new AsyncRequests(AddStudent.this,"Please wait","Adding student to the course");
                asyncRequests.addStudentToCourse(Integer.parseInt(rollNumber.getText().toString()), courseID, new LocalDBHelper(this),getApplicationContext(), new RequestsCallback() {
                    @Override
                    public void done(boolean b) {
                        if(b)
                            Toast.makeText(AddStudent.this, "Added", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(AddStudent.this, "Problem encountered while adding the roll number", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void done(User e) {

                    }

                    @Override
                    public void done(String s) {

                    }

                });
                break;
            case R.id.button_view:
                Intent intent = new Intent(AddStudent.this, ViewStudents.class);
                intent.putExtra("courseID", courseID);
                startActivity(intent);
                break;
            case R.id.button_import:
//                AsyncRequests asyncRequests1=new AsyncRequests(this,"Importing from file","Please wait...");
//                asyncRequests1.handleFileFetch(new LocalDBHelper(this),)
                m_ChosenDir="";
                m_NewFolderEnabled=true;
                DirectoryChooserDialog directoryChooserDialog=new DirectoryChooserDialog(AddStudent.this, new DirectoryChooserDialog.ChosenDirectoryListener() {
                    @Override
                    public void onChosenDir(String chosenDir) {
                        m_ChosenDir=chosenDir;
                        Toast.makeText(AddStudent.this,"Chosen file : "+m_ChosenDir,Toast.LENGTH_LONG).show();
                        AsyncRequests asyncRequests1=new AsyncRequests(AddStudent.this,"Importing from "+m_ChosenDir.substring(m_ChosenDir.lastIndexOf('/')),"Please wait...");
                        try {
                            BufferedReader br=new BufferedReader(new FileReader(m_ChosenDir));
                            asyncRequests1.handleFileFetch(
                                    new LocalDBHelper(AddStudent.this), br, courseID,getApplicationContext(), new RequestsCallback() {
                                        @Override
                                        public void done(boolean b) {

                                        }

                                        @Override
                                        public void done(User e) {

                                        }

                                        @Override
                                        public void done(String s) {
                                            errorLog.setText(s);
                                        }

                                    });
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
                // Toggle new folder button enabling
                directoryChooserDialog.setNewFolderEnabled(m_NewFolderEnabled);
                // Load directory chooser dialog for initial 'm_chosenDir' directory.
                // The registered callback will be called upon final directory selection.
                directoryChooserDialog.chooseDirectory(m_ChosenDir);
                m_NewFolderEnabled = ! m_NewFolderEnabled;
        }
    }
}
