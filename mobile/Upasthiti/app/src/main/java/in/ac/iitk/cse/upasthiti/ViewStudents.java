package in.ac.iitk.cse.upasthiti;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class ViewStudents extends ActionBarActivity{

    String courseID;
    ListView studentsLV;
    ArrayAdapter<String> adapter;
    TableLayout headerTable,contentTable;
//    List<String> students;
//    String[] data;
//            newTable;
//    TextView StudentView;

    private TableLayout addRowToTable(TableLayout table, String contentCol1, String contentCol2,String contentCol3) {
        TableRow row = new TableRow(getApplicationContext());

        TableRow.LayoutParams rowParams = new TableRow.LayoutParams();
        // Wrap-up the content of the row
        rowParams.height = TableRow.LayoutParams.WRAP_CONTENT;
        rowParams.width = TableRow.LayoutParams.WRAP_CONTENT;

        // The simplified version of the table of the picture above will have three columns
        // FIRST COLUMN
        TableRow.LayoutParams col1Params = new TableRow.LayoutParams();
        // Wrap-up the content of the row
        col1Params.height = TableRow.LayoutParams.WRAP_CONTENT;
        col1Params.width = TableRow.LayoutParams.WRAP_CONTENT;
        // Set the gravity to center the gravity of the column
        col1Params.gravity = Gravity.CENTER;
        TextView col1 = new TextView(getApplicationContext());
        col1.setText(contentCol1);
        col1.setTextColor(Color.parseColor("#212121"));
        row.addView(col1, col1Params);

        // SECOND COLUMN
        TableRow.LayoutParams col2Params = new TableRow.LayoutParams();
        // Wrap-up the content of the row
        col2Params.height = TableRow.LayoutParams.WRAP_CONTENT;
        col2Params.width = TableRow.LayoutParams.WRAP_CONTENT;
        // Set the gravity to center the gravity of the column
        col2Params.gravity = Gravity.CENTER;
        TextView col2 = new TextView(getApplicationContext());
        col2.setText(contentCol2);
        col2.setTextColor(Color.parseColor("#212121"));
        row.addView(col2, col2Params);

        // THIRD COLUMN
        TableRow.LayoutParams col3Params = new TableRow.LayoutParams();
        // Wrap-up the content of the row
        col3Params.height = TableRow.LayoutParams.WRAP_CONTENT;
        col3Params.width = TableRow.LayoutParams.WRAP_CONTENT;
        // Set the gravity to center the gravity of the column
        col3Params.gravity = Gravity.CENTER;
        TextView col3 = new TextView(getApplicationContext());
        col3.setText(contentCol3);
        col3.setTextColor(Color.parseColor("#212121"));
        row.addView(col3, col3Params);

        table.addView(row, rowParams);

        return table;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);
        Bundle bundle = getIntent().getExtras();
        courseID = bundle.getString("courseID");
        studentsLV=(ListView)findViewById(R.id.studentsListView);
        headerTable = (TableLayout)findViewById(R.id.headerTable);
        contentTable = (TableLayout)findViewById(R.id.contentTable);
        contentTable = addRowToTable(contentTable, "Sl. No.", "Roll No.", "Name");
        handleIntent(getIntent());
        AsyncRequests asyncRequests=new AsyncRequests(this,"Loading","Please wait");
        asyncRequests.viewCourseStudents(courseID, new LocalDBHelper(this), new RequestsCallback() {
            @Override
            public void done(boolean b) {

            }

            @Override
            public void done(User e) {

            }

            @Override
            public void done(String s) {
                Toast.makeText(ViewStudents.this, s, Toast.LENGTH_LONG).show();
//                StudentView.setText(s);
                StringTokenizer tokens = new StringTokenizer(s, ",");
                String[] array=new String[Integer.parseInt(tokens.nextToken())];
                int i=1;
                while(tokens.hasMoreTokens()){
                    String roll=tokens.nextToken();
                    String name=tokens.nextToken();
                    array[i-1]=name;
                    contentTable=addRowToTable(contentTable,i++ + "",roll,name);
                }
                adapter=new ArrayAdapter<String>(ViewStudents.this,android.R.layout.simple_list_item_1,array);
                studentsLV.setAdapter(adapter);
                studentsLV.setTextFilterEnabled(true);
                studentsLV.setVisibility(View.GONE);
                studentsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String name= (String) studentsLV.getItemAtPosition(position);
                        Cursor cursor=new LocalDBHelper(ViewStudents.this).getData(name);

                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ViewStudents.this);
                        dlgAlert.setTitle("Student details");
                        while (cursor.moveToNext()){
                            int roll=cursor.getInt(cursor.getColumnIndex("roll_no"));
                            String email=cursor.getString(cursor.getColumnIndex("email"));
                            String dep=cursor.getString(cursor.getColumnIndex("department"));
                            dlgAlert.setMessage("Name : "+name+"\nRoll Number : "+roll+"\nEmail : "+email+"\nDepartment : "+dep);

                        }
                        dlgAlert.setPositiveButton("OK",null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();
                        cursor.close();
                    }
                });
            }

        });
    }

    private void handleIntent(Intent intent) {
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            String query = intent.getStringExtra(SearchManager.QUERY);
//            handle search query
//            Cursor c = new LocalDBHelper(this).getWordMatches(query, null);
//            Cursor c = new LocalDBHelper(this).getStudentList(courseID);

//            students=new ArrayList<String>();
//            while (c.moveToNext()){
//                students.add(c.getString(0));
//            }
//            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,students);
//            studentsLV.setAdapter(arrayAdapter);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_students, menu);

//        Associate searchable configs
        SearchManager searchManager=(SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView=(SearchView)menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textListener= new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.equalsIgnoreCase("")) {
                    studentsLV.setVisibility(View.GONE);
                    contentTable.setVisibility(View.VISIBLE);
                }
                else {
                    studentsLV.setVisibility(View.VISIBLE);
                    contentTable.setVisibility(View.GONE);
                }
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText.equalsIgnoreCase("")){
                    studentsLV.setVisibility(View.GONE);
                    contentTable.setVisibility(View.VISIBLE);
                }
                else {
                    studentsLV.setVisibility(View.VISIBLE);
                    contentTable.setVisibility(View.GONE);
                }
                adapter.getFilter().filter(newText);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
