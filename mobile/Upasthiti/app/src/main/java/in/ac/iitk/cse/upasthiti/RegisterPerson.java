package in.ac.iitk.cse.upasthiti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;


public class RegisterPerson extends ActionBarActivity implements View.OnClickListener {

    LinearLayout switchInstructor;
    EditText userEmail, userName, userRoll, userDep;
    TextView promptName, promptRoll, promptDep;
    Button proceed;
    User userRegister;
    CheckBox c;
    boolean flag_checkbox=false;
    protected String strip(String string){
        String s=string.trim();
        s+="@";
        return string.substring(0,s.indexOf('@'));
    }
    protected boolean reject(String string){
//        if(string)
        String s=string.trim();
        int specialCharCount=0,atCount=0;
        for (char c : string.toCharArray()) {
            if (!Character.isLetterOrDigit(c))
                specialCharCount++;
        }
        for (char c : string.toCharArray()) {
            if (c=='@'||c=='.'){ atCount++; }

        }
        return atCount!=specialCharCount;
    }

    protected void authEmail(final String email){
        AsyncRequests asyncRequests=new AsyncRequests(this,"Please wait","We are looking you up in our servers");
        asyncRequests.fetchEmailDetails(email, new RequestsCallback() {
            @Override
            public void done(boolean b) {

            }

            @Override
            public void done(User user) {
                if(user==null){
                    Toast.makeText(RegisterPerson.this,"Currently offline",Toast.LENGTH_LONG).show();
                }else if(user.email.equalsIgnoreCase(email)){
                    flag_checkbox=true;
                    Toast.makeText(RegisterPerson.this, "Matching email found", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(RegisterPerson.this);
                    dlgAlert.setTitle("Details autofilled");
                    dlgAlert.setMessage("We may have your details");//Improve grammar here
                    dlgAlert.setPositiveButton("OKAY", null);
                    if(user.isInstructor) {
                        if(!c.isChecked())
                            c.toggle();
                        switchInstructor.setBackgroundResource(R.drawable.ripple_switch_on);
                        TextView t = (TextView) findViewById(R.id.checktext);
                        t.setText("I am an instructor");
                        promptRoll.setTextColor(Color.parseColor("#9e9e9e"));
                        userRoll.setEnabled(false);
                        userRoll.setCursorVisible(false);
                        userRoll.setHint("");
                        userName.setText(user.name);
                        userEmail.setText(user.email);
                        userDep.setText(user.dep);
                    }else {
                        if(c.isChecked())
                            c.toggle();
                        switchInstructor.setBackgroundResource(R.drawable.ripple_switch_off);
                        TextView t = (TextView) findViewById(R.id.checktext);
                        t.setText("I am a student");
                        promptRoll.setTextColor(Color.parseColor("#de000000"));
                        userRoll.setEnabled(true);
                        userRoll.setCursorVisible(true);
                        userName.setText(user.name);
                        userEmail.setText(user.email);
                        userDep.setText(user.dep);
                        userRoll.setText(Integer.toString(user.roll));
                    }
                }else {
//                    Toast.makeText(RegisterPerson.this, "No query found or nothing in database", Toast.LENGTH_SHORT).show();
                    final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(RegisterPerson.this);
                    dlgAlert.setTitle("Details not found");
                    dlgAlert.setMessage("Fill in your details manually");
                    dlgAlert.setPositiveButton("YES", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }
                userRegister=user;
            }

            @Override
            public void done(String s) {

            }


        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_person);
        switchInstructor = (LinearLayout) findViewById(R.id.instructor_check);
        switchInstructor.setBackgroundColor(Color.parseColor("#d500f9"));
        c=(CheckBox)findViewById(R.id.checkboxinstructor);
        userEmail = (EditText) findViewById(R.id.user_email);
        userName = (EditText) findViewById(R.id.user_name);
        userRoll = (EditText) findViewById(R.id.user_roll);
        userDep = (EditText) findViewById(R.id.user_department);
        proceed = (Button) findViewById(R.id.button_proceed_p);
        promptName = (TextView) findViewById(R.id.prompt_name);
        promptRoll = (TextView) findViewById(R.id.prompt_roll);
        promptDep = (TextView) findViewById(R.id.prompt_dep);
//        listener=userRoll.getKeyListener();
//        dataBase=new LocalDBHelper(this);
//        Intent intent=getIntent();
//        final LocalDBHelper dataBase=(LocalDBHelper)intent.getParcelableExtra("dataBase");
//
//        dataBase=new LocalDBHelper(this);
//        fetchFromCSV();
//        preferences = PreferenceManager.getDefaultSharedPreferences(RegisterPerson.this);
//        userEmail.setOnEditorActionListener(new EditText.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v,int actionId, KeyEvent event) {
//                    try {
//                        if (actionId==EditorInfo.IME_ACTION_DONE||actionId==EditorInfo.IME_ACTION_GO||actionId==EditorInfo.IME_ACTION_NEXT) {
//                            String usermail = userEmail.getText().toString();
//                            if(!reject(usermail))
//                            authEmail(strip(usermail));
//                            else {
//                                Toast.makeText(RegisterPerson.this, "You may have entered an invalid email id", Toast.LENGTH_SHORT).show();
//                                userEmail.setText("");
//                            }
//                                return true;
//                        }
//                    }catch (Exception e) {
//                        Toast.makeText(RegisterPerson.this, e.getMessage(), Toast.LENGTH_LONG).show();
//                        e.printStackTrace();
//                    }
//                Toast.makeText(RegisterPerson.this,"Nope, loser",Toast.LENGTH_LONG).show();
//                return false;
//            }
//        });
        proceed.setOnClickListener(this);
        switchInstructor.setOnClickListener(this);

        //db.close();
//        db = new LocalDBHelper(RegisterPerson.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_person, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//        protected void readCSV(LocalDBHelper db){
//        InputStream inputStream = RegisterPerson.this.getResources().openRawResource(R.raw.student_data );
//        List resultList = new ArrayList();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//        try {
//            String csvLine;
//            while ((csvLine = reader.readLine()) != null) {
//                String[] row = csvLine.split(",");
//                if(!row[3].equalsIgnoreCase("Not available")) {
//                        Toast.makeText(RegisterPerson.this,row[0],Toast.LENGTH_SHORT).show();
//                    db.createDb(row[3], row[1], Integer.parseInt(row[0]), row[2]);
//                resultList.add(row);
//                }
//            }
//        }
//        catch (IOException ex) {
//            throw new RuntimeException("Error in reading CSV file: "+ex);
//        }
//        finally {
//            try {
//                inputStream.close();
//            }
//            catch (IOException e) {
//                throw new RuntimeException("Error while closing input stream: "+e);
//            }
//        }
//
//    }

    //To-Do: Add instructor/student choice functonality
    //To-Do: Add POST functonality
    //To-Do: Add department drop-down functonality
    //Comment in any suggestions required
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.instructor_check:
//                Toast.makeText(this, "Switch touched", Toast.LENGTH_SHORT).show();
                c.toggle();
                flag_checkbox=true;
                if(c.isChecked()) {
//                    switchInstructor.setBackgroundResource(R.drawable.ripple_switch_on);
                    switchInstructor.setBackgroundColor(Color.parseColor("#ef5350"));
                    TextView t = (TextView) findViewById(R.id.checktext);
                    t.setText("I am an instructor");
                    t.setTextColor(Color.parseColor("#fafafa"));
                    promptRoll.setTextColor(Color.parseColor("#9e9e9e"));
                    userRoll.setHint("");
                    userRoll.setEnabled(false);
//                    userRoll.setFocusable(false);
//                    userRoll.setClickable(false);
//                    userRoll.setKeyListener(null);
                    userRoll.setCursorVisible(false);
//                    userRoll.setPressed(false);
                }else{
//                    switchInstructor.setBackgroundResource(R.drawable.ripple_switch_off);
                    switchInstructor.setBackgroundColor(Color.parseColor("#4a148c"));
                    TextView t = (TextView) findViewById(R.id.checktext);
                    t.setText("I am a student");
                    t.setTextColor(Color.parseColor("#fafafa"));
                    promptRoll.setTextColor(Color.parseColor("#de000000"));
                    userRoll.setHint("Eg: 14XXX");
                    userRoll.setEnabled(true);
//                    userRoll.setFocusable(true);
//                    userRoll.setClickable(true);
//                    userRoll.setKeyListener(listener);
                    userRoll.setCursorVisible(true);
//                    userRoll.setPressed(true);
                }
                break;
            case R.id.button_proceed_p:
                if(!flag_checkbox){
                    Toast.makeText(RegisterPerson.this, "Student or Instructor?", Toast.LENGTH_SHORT).show();
                    break;
                }
                if(userEmail.getText().toString().equals("")) {
                    Toast.makeText(RegisterPerson.this, "Enter your email", Toast.LENGTH_SHORT).show();
                    break;
                }
                if(userName.getText().toString().equals("")) {
                    Toast.makeText(RegisterPerson.this, "Enter your name", Toast.LENGTH_SHORT).show();
                    break;
                }
                if(!c.isChecked()) {
                    if(userRoll.getText().toString().equals("")) {
                        Toast.makeText(RegisterPerson.this, "Enter your roll number", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                if(userDep.getText().toString().equals("")) {
                    Toast.makeText(RegisterPerson.this, "Enter your department", Toast.LENGTH_SHORT).show();
                    break;
                }
                if(c.isChecked()){
                    userRegister=new User(userName.getText().toString(),userEmail.getText().toString(),userDep.getText().toString());
                }else {
                    userRegister=new User(userName.getText().toString(), userEmail.getText().toString(), Integer.parseInt(userRoll.getText().toString()), userDep.getText().toString());
                }
                Intent intent=new Intent(RegisterPerson.this,FingerCapture.class);
                SharedPreferences sharedPreferences=getSharedPreferences("User", Context.MODE_PRIVATE);
                SharedPreferences.Editor spEditor=sharedPreferences.edit();
                spEditor.putString("userName",userRegister.name);
                spEditor.putString("userEmail",userRegister.email);
                spEditor.putBoolean("isInstructor",userRegister.isInstructor);
                spEditor.putInt("userRoll",userRegister.roll);
                spEditor.putString("userDep",userRegister.dep);
                spEditor.apply();


                startActivity(intent);

        }
    }
}
